﻿using Microsoft.Extensions.DependencyInjection;

namespace Digit.Modularity
{
    public interface IModule
    {
        void Configure(IServiceCollection services);
        void Start();
        void Stop();
    }
}
