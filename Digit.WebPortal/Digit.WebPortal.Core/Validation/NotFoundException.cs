﻿using System.Net;

namespace Digit.WebPortal.Core.Validation
{
    public class NotFoundException : DigitAppException
    {
        public NotFoundException()
            : base("Object not found", HttpStatusCode.NotFound)
        {
        }
    }
}
