﻿using System.Net;

namespace Digit.WebPortal.Core.Validation
{
    public class InvalidDtoException : DigitAppException
    {
        public InvalidDtoException()
            : base("Invalid request body", HttpStatusCode.BadRequest)
        {
        }
    }
}
