﻿using Microsoft.AspNetCore.JsonPatch;
using System;

namespace Digit.WebPortal.Core.Validation
{
    public static class PatchHelper
    {
        public static void Apply<T>(JsonPatchDocument<T> patchDoc, T target)  where T : class
        {
            try
            {
                patchDoc.ApplyTo(target);
            }
            catch (Exception ex)
            {
                throw new ValidationException(ex.Message);
            }
        }
    }
}
