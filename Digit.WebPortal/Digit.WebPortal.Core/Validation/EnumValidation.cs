﻿using System;

namespace Digit.WebPortal.Core.Validation
{
    public static class EnumValidation
    {
        public static void CheckRange(Type enumType, int value, string fieldName)
        {
            if (!Enum.IsDefined(enumType, value))
            {
                throw new ValidationException($"{fieldName} out of range");
            }
        }
    }
}
