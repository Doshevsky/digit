﻿using System.Net;

namespace Digit.WebPortal.Core.Validation
{
    public class ValidationException : DigitAppException
    {
        public ValidationException(string message, HttpStatusCode code = HttpStatusCode.BadRequest)
            : base(message, code)
        {
        }
    }
}
