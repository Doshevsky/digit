﻿using System;

namespace Digit.WebPortal.Core.Validation
{
    public static class DateTimeValidation
    {
        public static void CheckNonDefault(DateTime value, string filedName)
        {
            if (value == DateTime.MinValue)
            {
                throw new ValidationException($"Invalid {filedName}");
            }
        }
    }
}
