﻿namespace Digit.WebPortal.Core.Validation
{
    public static class StringValidation
    {
        public static void CheckEmptyOrSpecifiedCharacters(int n, string value, string fieldName)
        {
            if (!string.IsNullOrEmpty(value) && value.Length > n)
            {
                throw new ValidationException($"{fieldName} must contain up to {n} characters");
            }
        }
        public static void CheckEmptyOr50Characters(string value, string fieldName)
        {
            CheckEmptyOrSpecifiedCharacters(50, value, fieldName);
        }
        public static void CheckEmptyOr250Characters(string value, string fieldName)
        {
            CheckEmptyOrSpecifiedCharacters(250, value, fieldName);
        }

        public static void CheckNonEmptyAndSpecifiedCharacters(int n, string value, string fieldName)
        {
            if (string.IsNullOrWhiteSpace(value) || value.Length > n)
            {
                throw new ValidationException($"{fieldName} cannot be empty and can contain up to {n} characters");
            }
        }
        public static void CheckNonEmptyAnd50Characters(string value, string fieldName)
        {
            CheckNonEmptyAndSpecifiedCharacters(50, value, fieldName);
        }
        public static void CheckNonEmptyAnd250Characters(string value, string fieldName)
        {
            CheckNonEmptyAndSpecifiedCharacters(250, value, fieldName);
        }
        public static void CheckNonEmptyAnd350Characters(string value, string fieldName)
        {
            CheckNonEmptyAndSpecifiedCharacters(350, value, fieldName);
        }
    }
}
