﻿namespace Digit.WebPortal.Core.Validation
{
    public static class Require
    {
        public static void Dto(object value)
        {
            if (value == null)
            {
                throw new InvalidDtoException();
            }
        }
        public static void DataObject(object value)
        {
            if (value == null)
            {
                throw new NotFoundException();
            }
        }
    }
}
