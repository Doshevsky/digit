﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;

namespace Digit.WebPortal.Core
{
    public interface IServiceBootstrapper
    {
        string ServiceId { get; }
        Profile MapperProfile { get; }

        void Initialize(IServiceCollection container);
    }
}
