﻿namespace Digit.WebPortal.Core
{
    public interface IDbContext
    {
        void ExecuteSqlCommand(string sql);
        void SaveChanges();
    }
}
