﻿using System;
using System.Net;

namespace Digit.WebPortal.Core
{
    public abstract class DigitAppException : Exception
    {
        protected DigitAppException(string message, HttpStatusCode code)
            : base(message)
        {
            Code = code;
        }

        public HttpStatusCode Code { get; }
    }
}
