﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using System.IO;

namespace Digit.WebPortal.Service.Controllers
{
    public class HomeController : Controller
    {
        public HomeController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }

        public IActionResult Index()
        {
            string path = Path.Combine(_hostingEnvironment.WebRootPath, "index.html");
            string content = System.IO.File.ReadAllText(path);
            return Content(content, "text/html");
        }

        private readonly IHostingEnvironment _hostingEnvironment;
    }
}
