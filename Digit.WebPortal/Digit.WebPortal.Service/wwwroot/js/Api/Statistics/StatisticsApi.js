class StatisticsApi extends ApiBase {
    static visitsByDay(date, f) {
        let url = '/api/customers/statistics/visitsbyday?date=' + date;

        ApiBase.get(url, function (data) {
            f(data.map(function (item) {
                return new Customer(item);
            }));
        });
    }
}