class CommentsApi extends ApiBase {
    static getById(id, f) {
        ApiBase.get('/api/customers/comments/' + id, function (data) {
            f(new Comment(data));
        });
    }

    static put(id, data, f) {
        ApiBase.put('/api/customers/comments/' + id, data, f);
    }

    static delete(id, f) {
        ApiBase.delete('/api/customers/comments/' + id, f);
    }
}