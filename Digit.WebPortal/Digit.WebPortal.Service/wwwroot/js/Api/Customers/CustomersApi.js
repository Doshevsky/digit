class CustomersApi extends ApiBase {
    static get(statusId, sortType, searchQuery, f) {
        let url = '/api/customers';

        if (statusId != null) {
            url += '?status=' + statusId;
        }

        if (sortType != null) {
            url += '?sort=' + sortType;
        }

        if (searchQuery != null) {
            url += '?searchQuery=' + searchQuery;
        }

        ApiBase.get(url, function (data) {
            f(data.map(function (item) {
                return new Customer(item);
            }));
        });
    }

    static getById(id, f) {
        ApiBase.get('/api/customers/' + id, function (data) {
            f(new Customer(data));
        });
    }

    static post(data, f) {
        ApiBase.post('/api/customers', data, f);
    }

    static put(id, data, f) {
        ApiBase.put('/api/customers/' + id, data, f);
    }

    static delete(id, f) {
        ApiBase.delete('/api/customers/' + id, f);
    }

    static setStatus(customerId, status, f) {
        let url = '/api/customers/' + customerId;
        let data = [{
            "op": "replace",
            "path": "/status",
            "value": status
        }];
        ApiBase.patch(url, data, f);
    }

    static setAction(customerId, data, f) {
        let url = '/api/customers/' + customerId + '/action';
        ApiBase.post(url, data, f);
    }

    static deleteAction(customerId, f) {
        let url = '/api/customers/' + customerId + '/action';
        ApiBase.delete(url, f);
    }

    static addComment(customerId, data, f) {
        let url = '/api/customers/' + customerId + '/comments';
        ApiBase.post(url, data, f);
    }

    static addVisit(customerId, data, f) {
        let url = '/api/customers/' + customerId + '/visits';
        ApiBase.post(url, data, f);
    }

    static addPayment(customerId, data, f) {
        let url = '/api/customers/' + customerId + '/payments';
        ApiBase.post(url, data, f);
    }

    static addRepresentative(customerId, data, f) {
        let url = '/api/customers/' + customerId + '/representatives';
        ApiBase.post(url, data, f);
    }
}