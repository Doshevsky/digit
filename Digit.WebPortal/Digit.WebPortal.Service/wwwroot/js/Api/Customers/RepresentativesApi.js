class RepresentativesApi extends ApiBase {
    static getById(id, f) {
        ApiBase.get('/api/customers/representatives/' + id, function (data) {
            f(new Representative(data));
        });
    }

    static put(id, data, f) {
        ApiBase.put('/api/customers/representatives/' + id, data, f);
    }

    static delete(id, f) {
        ApiBase.delete('/api/customers/representatives/' + id, f);
    }
}