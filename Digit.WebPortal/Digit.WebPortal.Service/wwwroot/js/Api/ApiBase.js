class ApiBase {
    static get(path, f) {
        axios.get(path)
            .then(function (response) {
                f(response.data);
            })
            .catch(function (error) {
                ApiBase.onError(error);
            });
    }

    static post(path, data, f) {
        axios.post(path, data)
            .then(function (response) {
                f(response.data);
            })
            .catch(function (error) {
                ApiBase.onError(error);
            });
    }

    static put(path, data, f) {
        axios.put(path, data)
            .then(function (response) {
                f(response.data);
            })
            .catch(function (error) {
                ApiBase.onError(error);
            });
    }

    static patch(path, data, f) {
        axios.patch(path, data)
            .then(function (response) {
                f(response.data);
            })
            .catch(function (error) {
                ApiBase.onError(error);
            });
    }

    static delete(path, f) {
        axios.delete(path)
            .then(function (response) {
                f();
            })
            .catch(function (error) {
                ApiBase.onError(error);
            });
    }

    static onError(error) {
        console.log(error);

        let message = error.response.data ? error.response.data : error;
        alert(message);
    }
}