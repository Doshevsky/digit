class AuthApi extends ApiBase {
    static getCurrentUser(f) {
        axios.get('/auth/user')
            .then(function (response) {
                f(new User(response.data));
            })
            .catch(function (error) {
                f(null);
            });
    }

    static login(data, f) {
        ApiBase.post('/auth/login', data, f);
    }

    static logout(f) {
        ApiBase.post('/auth/logout', null, f);
    }

    static register(data, f) {
        ApiBase.post('/auth/register', data, f);
    }
}