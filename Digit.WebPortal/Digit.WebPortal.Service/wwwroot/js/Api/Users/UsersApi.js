class UsersApi extends ApiBase {
    static get(f) {
        ApiBase.get('/api/users', function (data) {
            f(data.map(function (item) {
                return new User(item);
            }));
        });
    }

    static getById(id, f) {
        ApiBase.get('/api/users/' + id, function (data) {
            f(new User(data));
        });
    }

    static put(id, data, f) {
        ApiBase.put('/api/users/' + id, data, f);
    }

    static delete(id, f) {
        ApiBase.delete('/api/users/' + id, f);
    }
}