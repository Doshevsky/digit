class MainMenu extends React.Component {
    render() {
        return (
            <div className="main-menu">
                <IconButton href="/" text="Главная" icon="home" size="lg" type="primary" />
                <IconButton href="/users" text="Пользователи" icon="user" size="lg" type="primary" />
                <IconButton href="/customers" text="Клиенты" icon="th" size="lg" type="primary" />
            </div>
        );
    }
}