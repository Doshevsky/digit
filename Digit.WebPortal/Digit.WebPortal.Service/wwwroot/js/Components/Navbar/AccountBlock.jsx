class AccountBlock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loaded: false,
            user: undefined
        };

        this.logoutClick = this.logoutClick.bind(this);
    }

    logoutClick() {
        AuthApi.logout(function () {
            window.location.replace('/home');
        });
    }

    componentDidMount() {
        let self = this;
        AuthApi.getCurrentUser(function (data) {
            self.setState({
                user: data,
                loaded: true
            });
        });
    }

    render() {
        if (!this.state.loaded) {
            return <p></p>;
        }

        if (this.state.user) {
            return (
                <ul className="nav navbar-nav navbar-right">
                    <li className="dropdown">
                        <a className="dropdown-toggle" data-toggle="dropdown" href="#">
                            {this.state.user.username}
                            <span class="caret"></span>
                        </a>
                        <ul className="dropdown-menu">
                            <li><a href="#">Профиль</a></li>
                            <li><a href="#" onClick={this.logoutClick}>Выход</a></li>
                        </ul>
                    </li>
                </ul>
            );
        }

        return (
            <ul className="nav navbar-nav navbar-right">
                <li><a href="/login">Вход</a></li>
                <li><a href="/register">Регистрация</a></li>
            </ul>
        );
    }
}