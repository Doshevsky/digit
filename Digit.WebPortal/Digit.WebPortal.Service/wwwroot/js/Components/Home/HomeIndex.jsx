class HomeIndex extends React.Component {
    render() {
        return (
            <div>
                <h3>Информационно - аналитическая система Digit</h3>
                <p><a href="http://digit.center">http://digit.center</a></p>
                <p><a href="https://vk.com/digitcenter">https://vk.com/digitcenter</a></p>
                <p><a href="https://www.facebook.com/centerdigit">https://www.facebook.com/centerdigit</a></p>

                <h3>Статистика</h3>
                <p><a href="/stat/visits">Посещения</a></p>
            </div>
        );
    }
}