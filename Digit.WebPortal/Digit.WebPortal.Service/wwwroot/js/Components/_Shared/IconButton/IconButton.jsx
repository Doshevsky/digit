class IconButton extends React.Component {
    render() {
        let buttonSizeClass = this.props.size ?
            " btn-" + this.props.size : "";

        let buttonClass = "btn icon-button" +
            buttonSizeClass +
            " btn-" + this.props.type;

        let iconClass = "glyphicon glyphicon-" + this.props.icon;

        return (
            <a href={this.props.href} className={buttonClass} onClick={this.props.handler}>
                <span className={iconClass}></span> {this.props.text}
            </a>
        );
    }
}