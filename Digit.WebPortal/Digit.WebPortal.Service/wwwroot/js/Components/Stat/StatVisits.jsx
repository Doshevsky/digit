class StatVisits extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            customers: undefined,
            input: moment.utc().format("DD.MM.YYYY")
        };

        this.loadData = this.loadData.bind(this);
        this.buttonClick = this.buttonClick.bind(this);
        this.inputChange = this.inputChange.bind(this);
    }

    loadData(date) {
        let self = this;
        StatisticsApi.visitsByDay(date.toJSON(), function (data) {
            self.setState({ customers: data });
        });
    }

    buttonClick(event) {
        let date = moment.utc().subtract(event.target.id, 'days')
        this.setState({ input: date.format("DD.MM.YYYY") });
        this.loadData(date);
    }

    inputChange(event) {
        this.setState({ input: event.target.value });

        let date = moment.utc(event.target.value, 'DD.MM.YYYY', true);
        if (date.isValid()) {
            this.loadData(date);
        }
    }

    componentDidMount() {
        this.loadData(moment.utc());
    }

    render() {
        return (
            <div>
                <div className="row">
                    <div className="content-header-left">
                        <div>
                            <div className="btn-group" role="group">
                                <button id={0} type="button" className="btn btn-default btn-sm"
                                    onClick={this.buttonClick}>Сегодня</button>
                                <button id={1} type="button" className="btn btn-default btn-sm"
                                    onClick={this.buttonClick}>Вчера</button>
                                <button id={2} type="button" className="btn btn-default btn-sm"
                                    onClick={this.buttonClick}>-2</button>
                                <button id={3} type="button" className="btn btn-default btn-sm"
                                    onClick={this.buttonClick}>-3</button>
                                <button id={4} type="button" className="btn btn-default btn-sm"
                                    onClick={this.buttonClick}>-4</button>
                                <button id={5} type="button" className="btn btn-default btn-sm"
                                    onClick={this.buttonClick}>-5</button>
                            </div>

                            &nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;&nbsp;

                            <div className="btn-group" role="group">
                                <input className="form-control input-sm" type="text"
                                    value={this.state.input} onChange={this.inputChange} />
                            </div>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <CustomersList customers={this.state.customers} />
                </div>
            </div>
        );
    }
}