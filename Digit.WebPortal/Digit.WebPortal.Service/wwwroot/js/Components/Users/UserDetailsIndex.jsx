class UserDetailsIndex extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: null,
            email: null,
            role: null,
            loaded: false
        };

        this.inputChange = this.inputChange.bind(this);
        this.roleChange = this.roleChange.bind(this);
        this.saveClick = this.saveClick.bind(this);
        this.deleteClick = this.deleteClick.bind(this);
    }

    inputChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    roleChange(input) {
        let option = input.target[input.target.selectedIndex];
        this.setState({ role: new Role(option.value) });
    }

    saveClick() {
        let dto = {
            username: this.state.username,
            email: this.state.email,
            role: this.state.role.id
        };

        UsersApi.put(this.props.params.id, dto, function () {
            window.location.replace('/users');
        });
    }

    deleteClick() {
        UsersApi.delete(this.props.params.id, function () {
            window.location.replace('/users');
        });
    }

    componentDidMount() {
        let self = this;

        UsersApi.getById(self.props.params.id, function (data) {
            self.setState({
                username: data.username,
                email: data.email,
                role: data.role,
                loaded: true
            });
        });
    }

    render() {
        if (!this.state.loaded) {
            return <p>Loading...</p>;
        }

        return (
            <div>
                <div className="row">
                    <div>
                        <div className="content-header-left">
                            <h3 className="content-header-text">
                                Пользователь #{this.props.params.id}
                            </h3>
                        </div>

                        <div className="content-header-right">
                            <IconButton icon="ok" text="Сохранить" type="success" handler={this.saveClick} />
                            <IconButton icon="trash" type="danger" handler={this.deleteClick} />
                        </div>

                        <div className="clearfix"></div>
                        <hr className="content-header-hr" />
                    </div>
                </div>

                <div className="row">
                    <form className="form-horizontal">
                        <div className="form-group">
                            <label className="col-sm-2 control-label">Имя пользователя</label>
                            <div className="col-sm-4">
                                <input name="username" className="form-control" type="text" value={this.state.username} onChange={this.inputChange} />
                            </div>
                        </div>

                        <div className="form-group">
                            <label className="col-sm-2 control-label">Email</label>
                            <div className="col-sm-4">
                                <input name="email" className="form-control" type="text" value={this.state.email} onChange={this.inputChange} />
                            </div>
                        </div>

                        <div className="form-group">
                            <label className="col-sm-2 control-label">Роль</label>
                            <div className="col-sm-2">
                                <select className="form-control" value={this.state.role.id} onChange={this.roleChange}> {
                                    Object.keys(RolesList).slice(1).map(function (roleId) {
                                        return (
                                            <option value={roleId}>{RolesList[roleId]}</option>
                                        );
                                    })}
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}