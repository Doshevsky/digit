class UsersIndex extends React.Component {
    constructor(props) {
        super(props);
        this.state = { users: undefined };
    }

    componentDidMount() {
        let self = this;
        UsersApi.get(function (data) {
            self.setState({ users: data });
        });
    }

    render() {
        if (!this.state.users) {
            return <p>Loading...</p>;
        }

        return (
            <div>
                <div className="row">
                    <div className="content-header-left">
                        <h4>Пользователи</h4>
                    </div>
                </div>

                <div className="row">
                    <div className="table-responsive">
                        <table className="table table-hover">
                            <thead>
                                <tr>
                                    <th>Имя</th>
                                    <th>Email</th>
                                    <th>Роль</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody> {
                                this.state.users.map(function (user) {
                                    return (
                                        <tr>
                                            <td>{user.username}</td>
                                            <td>{user.email}</td>
                                            <td>{user.role.title}</td>
                                            <td className="text-right">
                                                <IconButton href={"/users/id" + user.id} icon="option-horizontal" size="xs" type="primary" />
                                            </td>
                                        </tr>
                                    );
                                })
                            }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}