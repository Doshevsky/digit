class CustomersListParameters extends React.Component {
    constructor(props) {
        super(props);

        this.buttonClick = this.buttonClick.bind(this);
        this.inputChange = this.inputChange.bind(this);

        this.state = {
            searchQuery: "",
        };
    }

    buttonClick(event) {
        switch (event.target.name) {
            case "status":
                this.props.onParametersUpdate(event.target.id, null, null);
                break;
            case "sort":
                this.props.onParametersUpdate(null, event.target.id, null);
                break;
        }
    }

    inputChange(event) {
        this.setState({ [event.target.name]: event.target.value });
        this.props.onParametersUpdate(null, null, event.target.value);
    }

    render() {
        let self = this;
        return (
            <div>
                <div className="btn-group" role="group">
                    <button name="status" id={null} type="button" className="btn btn-default btn-sm"
                        onClick={self.buttonClick}>Все</button> {
                        Object.keys(StatusesList).map(function (statusId) {
                            return (
                                <button name="status" id={statusId} type="button" className="btn btn-default btn-sm"
                                    onClick={self.buttonClick}>{StatusesList[statusId]}</button>
                            );
                        })}
                </div>

                &nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;

                <div className="btn-group" role="group">
                    <button name="sort" id={1} type="button" className="btn btn-default btn-sm"
                        onClick={self.buttonClick}>Имя</button>
                    <button name="sort" id={2} type="button" className="btn btn-default btn-sm"
                        onClick={self.buttonClick}>Напоминание</button>
                </div>

                &nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;

                <div className="btn-group" role="group">
                    <input name="searchQuery" className="form-control input-sm" type="text" value={this.state.searchQuery} onChange={this.inputChange} />
                </div>
            </div>
        );
    }
}