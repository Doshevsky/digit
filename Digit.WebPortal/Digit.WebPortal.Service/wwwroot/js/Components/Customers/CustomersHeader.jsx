class CustomersHeader extends React.Component {
    render() {
        return (
            <div>
                <div className="content-header-left">
                    <CustomersListParameters onParametersUpdate={this.props.onParametersUpdate} />
                </div>

                <div className="content-header-right">
                    <IconButton href="/customers/edit" text="Добавить" icon="plus" type="primary" />
                </div>
            </div>
        );
    }
}