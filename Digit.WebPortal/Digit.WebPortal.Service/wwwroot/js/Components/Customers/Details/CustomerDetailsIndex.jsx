class CustomerDetailsIndex extends React.Component {
    constructor(props) {
        super(props);
        this.state = { customer: null };
        this.update = this.update.bind(this);
    }

    update() {
        this.componentDidMount();
    }

    componentDidMount() {
        let self = this;
        CustomersApi.getById(self.props.params.id, function(data) {
            self.setState({ customer: data });
        });
    }

    render() {
        if (!this.state.customer) {
            return <p>Loading...</p>;
        }

        return (
            <div>
                <div className="row">
                    <CustomerDetailsHeader customer={this.state.customer} />
                </div>

                <div className="row">
                    <div className="col-md-6 customer-details-left-column">
                        <ContactsList contacts={this.state.customer.contacts} />
                        <CustomerDetailsChannel channel={this.state.customer.channel} />
                        <p><i>{this.state.customer.information}</i></p>

                        <div className="top-buffer">
                            <RepresentativesHeader customerId={this.state.customer.id} />
                            <RepresentativesList customerId={this.state.customer.id} representatives={this.state.customer.representatives} />
                        </div>
                    </div>

                    <div className="col-md-6 customer-details-right-column">
                        <ActionBlock customer={this.state.customer} />
                        <CustomerDetailsTabs customer={this.state.customer} handler={this.update} />
                    </div>
                </div>
            </div>
        );
    }
}