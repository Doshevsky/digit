class AddVisitModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = { date: '' };

        this.dateChange = this.dateChange.bind(this);
        this.saveClick = this.saveClick.bind(this);
        this.todayClick = this.todayClick.bind(this);
        this.saveDate = this.saveDate.bind(this);
    }

    dateChange(event) {
        this.setState({ date: event.target.value });
    }

    saveClick() {
        let date = moment.utc(this.state.date, 'DD.MM.YYYY', true);
        this.saveDate(date);
    }

    todayClick() {
        let date = moment.utc().startOf('day');
        this.saveDate(date);
    }

    saveDate(date) {
        if (date.isValid()) {
            let visit = new Visit();
            visit.start = date.toJSON();
            visit.end = visit.start;

            let self = this;
            CustomersApi.addVisit(this.props.customerId, visit, function () {
                self.props.handler();
            });
        }
    }

    render() {
        return (
            <div>
                <div id="addVisitModal" className="modal fade" role="dialog">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal">&times; </button>
                                <h4 className="modal-title">Посещение</h4>
                            </div>

                            <div className="modal-body">
                                <form className="form-horizontal">
                                    <div className="form-group">
                                        <label className="col-sm-2 control-label">Дата</label>

                                        <div className="col-sm-4">
                                            <input type="text" className="form-control" placeholder="25.09.2017"
                                                value={this.state.date} onChange={this.dateChange} />
                                        </div>

                                        <button type="button" className="btn btn-primary" data-dismiss="modal"
                                            onClick={this.todayClick}>Сегодня</button>
                                    </div>
                                </form>
                            </div>

                            <div className="modal-footer">
                                <button type="button" className="btn btn-default" data-dismiss="modal">Закрыть</button>
                                <button type="button" className="btn btn-primary" data-dismiss="modal"
                                    onClick={this.saveClick}>Сохранить</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}