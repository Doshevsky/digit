class VisitsListItem extends React.Component {
    constructor(props) {
        super(props);
        this.deleteClick = this.deleteClick.bind(this);
    }

    deleteClick() {
        let self = this;
        VisitsApi.delete(this.props.visit.id, function() {
            self.props.handler();
        });
    }

    render() {
        return (
            <tr>
                <td>{this.props.visit.date}</td>
                <td>{this.props.visit.start}</td>
                <td>{this.props.visit.end}</td>
                <td>{this.props.visit.duration}</td>
                <td className="text-right">
                    <IconButton icon="trash" size="xs" type="danger" handler={this.deleteClick} />
                </td>
            </tr>
        );
    }
}