class VisitsList extends React.Component {
    render() {
        let self = this;
        return (
            <div id="visits" className="tab-pane fade customer-details-tab-block">
                <div className="table-responsive">
                    <table className="table table-hover">
                        <thead>
                            <tr>
                                <th>Дата</th>
                                <th>Начало</th>
                                <th>Конец</th>
                                <th>&#10095;</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody> {
                            this.props.visits.map(function(visit) {
                                return <VisitsListItem visit={visit} handler={self.props.handler} />;
                            })
                        }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}