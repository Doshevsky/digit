class CustomerDetailsDeleteModal extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        CustomersApi.delete(this.props.customer.id, function () {
            window.location.replace('/customers');
        });
    }

    render() {
        return (
            <div>
                <div id="deleteModal" className="modal fade" role="dialog">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal">&times; </button>
                                <h4 className="modal-title">Удаление клиента</h4>
                            </div>
                            <div className="modal-body">
                                <p>Вы действительно хотите удалить клиента?</p>
                                <p>#{this.props.customer.id}, {this.props.customer.fullName}</p>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-default" data-dismiss="modal">Отмена</button>
                                <button type="button" className="btn btn-danger" data-dismiss="modal" onClick={this.handleClick}>Удалить</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}