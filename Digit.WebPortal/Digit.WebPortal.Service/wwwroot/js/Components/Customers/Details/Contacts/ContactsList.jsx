class ContactsList extends React.Component {
    render() {
        return (
            <div> {
                this.props.contacts.map(function (contact) {
                    return <ContactsListItem contact={contact} />;
                })
            }
            </div>
        );
    }
}