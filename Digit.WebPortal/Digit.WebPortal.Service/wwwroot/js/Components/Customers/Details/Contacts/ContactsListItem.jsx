class ContactsListItem extends React.Component {
    render() {
        return (
            <p>
                <strong>{this.props.contact.title}: </strong>
                {this.props.contact.value}
            </p>
        );
    }
}