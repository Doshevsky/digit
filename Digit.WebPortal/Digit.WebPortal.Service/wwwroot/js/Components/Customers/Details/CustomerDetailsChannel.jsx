class CustomerDetailsChannel extends React.Component {
    render() {
        if (!this.props.channel) {
            return <p></p>;
        }

        return (
            <p><strong>Канал: </strong>{this.props.channel.title}</p>
        );
    }
}