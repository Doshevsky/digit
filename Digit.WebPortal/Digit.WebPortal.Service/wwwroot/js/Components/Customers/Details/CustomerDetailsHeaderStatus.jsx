class CustomerDetailsHeaderStatus extends React.Component {
    constructor(props) {
        super(props);

        this.state = { selectedStatusId: this.props.customer.status.id };
        this.statusChange = this.statusChange.bind(this);
    }

    statusChange(input) {
        let self = this;
        let option = input.target[input.target.selectedIndex];
        CustomersApi.setStatus(this.props.customer.id, option.id, function () {
            self.setState({ selectedStatusId: option.id });
        });
    }

    render() {
        let self = this;
        return (
            <div className="customer-details-header-status">
                <select className="form-control" onChange={this.statusChange}> {
                    Object.keys(StatusesList).map(function (statusId) {
                        return (
                            <option id={statusId} selected={statusId == self.state.selectedStatusId}>{StatusesList[statusId]}</option>
                        );
                    })}
                </select>
            </div>
        );
    }
}