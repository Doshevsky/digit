class RepresentativesList extends React.Component {
    render() {
        let customerId = this.props.customerId;
        return (
            <div> {
                this.props.representatives.map(function (representative) {
                    return <RepresentativesListItem customerId={customerId} representative={representative} />;
                })
            }
            </div>
        );
    }
}