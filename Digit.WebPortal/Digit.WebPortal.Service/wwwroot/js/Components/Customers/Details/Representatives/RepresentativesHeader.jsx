class RepresentativesHeader extends React.Component {
    render() {
        let addPath = "/customers/id" + this.props.customerId + '/representatives/edit';
        return (
            <div>
                <div className="content-header-left">
                    <h4 class="content-header-text">Представители</h4>
                </div>

                <div className="content-header-right">
                    <IconButton href={addPath} icon="plus" size="sm" type="primary" />
                </div>

                <div className="clearfix"></div>
                <hr className="content-header-hr" />
            </div>
        );
    }
}