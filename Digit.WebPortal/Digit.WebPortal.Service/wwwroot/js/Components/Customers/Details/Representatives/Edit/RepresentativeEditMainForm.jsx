class RepresentativeEditMainForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: this.props.representative.firstName,
            middleName: this.props.representative.middleName,
            lastName: this.props.representative.lastName,
            comment: this.props.representative.comment,
        };

        this.inputChange = this.inputChange.bind(this);
    }

    inputChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    render() {
        return (
            <div>
                <form className="form-horizontal">
                    <div className="form-group">
                        <label className="col-sm-2 control-label">Фамилия</label>
                        <div className="col-sm-8">
                            <input name="lastName" className="form-control" type="text" value={this.state.lastName} onChange={this.inputChange} />
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="col-sm-2 control-label">Имя</label>
                        <div className="col-sm-8">
                            <input name="firstName" className="form-control" type="text" value={this.state.firstName} onChange={this.inputChange} />
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="col-sm-2 control-label">Отчество</label>
                        <div className="col-sm-8">
                            <input name="middleName" className="form-control" type="text" value={this.state.middleName} onChange={this.inputChange} />
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="col-sm-2 control-label">Комментарий</label>
                        <div className="col-sm-8">
                            <textarea name="comment" className="form-control" rows="3" value={this.state.comment} onChange={this.inputChange}></textarea>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}