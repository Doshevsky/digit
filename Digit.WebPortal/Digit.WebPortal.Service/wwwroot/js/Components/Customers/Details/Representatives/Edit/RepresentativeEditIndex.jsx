class RepresentativeEditIndex extends React.Component {
    constructor(props) {
        super(props);
        this.state = { representative: null };

        this.save = this.save.bind(this);
        this.delete = this.delete.bind(this);
    }

    componentDidMount() {
        let representativeId = this.props.params.id;
        if (representativeId) {
            let self = this;
            RepresentativesApi.getById(representativeId, function (data) {
                self.setState({ representative: data });
            });
        }
        else {
            this.setState({ representative: new Representative() });
        }
    }

    save() {
        let representative = this.state.representative;

        representative.firstName = this.refs.mainForm.state.firstName;
        representative.middleName = this.refs.mainForm.state.middleName;
        representative.lastName = this.refs.mainForm.state.lastName;
        representative.comment = this.refs.mainForm.state.comment;

        representative.contacts = [];
        this.refs.contactsForm.state.contacts.map(function (contact) {
            if (contact.value) {
                representative.contacts.push(contact);
            }
        });

        let customerId = this.props.params.customerid;
        if (representative.id != undefined) {
            RepresentativesApi.put(representative.id, representative, function () {
                window.location.replace('/customers/id' + customerId);
            })
        }
        else {
            CustomersApi.addRepresentative(customerId, representative, function (id) {
                window.location.replace('/customers/id' + customerId);
            })
        }
    }

    delete() {
        let self = this;
        RepresentativesApi.delete(this.state.representative.id, function () {
            window.location.replace('/customers/id' + self.props.params.customerid);
        });
    }

    render() {
        if (!this.state.representative) {
            return <p>Loading...</p>;
        }

        let deleteHandler = this.state.representative.id != undefined ? this.delete : null;

        return (
            <div>
                <div className="row">
                    <RepresentativeEditHeader representative={this.state.representative} onSave={this.save} onDelete={deleteHandler} />
                </div>

                <div className="row">
                    <RepresentativeEditMainForm representative={this.state.representative} ref="mainForm" />
                    <CustomerEditContactsForm contacts={this.state.representative.contacts} ref="contactsForm" />
                </div>
            </div>
        );
    }
}