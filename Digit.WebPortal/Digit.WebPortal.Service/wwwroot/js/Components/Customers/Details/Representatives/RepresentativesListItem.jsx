class RepresentativesListItem extends React.Component {
    render() {
        let editPath = "/customers/id" + this.props.customerId + '/representatives/edit/id' + this.props.representative.id;
        let comment = this.props.representative.comment;
        return (
            <div className="customer-details-representative-item">
                <p><strong>{this.props.representative.fullName}</strong></p>

                <ContactsList contacts={this.props.representative.contacts} />

                <div className="block-right">
                    <IconButton href={editPath} icon="pencil" size="xs" type="primary" />
                </div>

                <p><i>{comment ? comment : '\u200D'}</i></p>

                <hr />
            </div>
        );
    }
}