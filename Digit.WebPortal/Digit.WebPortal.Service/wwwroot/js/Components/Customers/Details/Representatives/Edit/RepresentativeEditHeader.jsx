class RepresentativeEditHeader extends React.Component {
    render() {
        let title = this.props.representative.id ? 'Представитель #' + this.props.representative.id : 'Новый представитель';

        let deleteButton;
        if (this.props.onDelete) {
            deleteButton = <IconButton icon="trash" text="Удалить" type="danger" handler={this.props.onDelete} />;
        }
        else {
            deleteButton = <p></p>;
        }

        return (
            <div>
                <div className="content-header-left">
                    <h3 className="content-header-text">
                        {title}
                    </h3>
                </div>

                <div className="content-header-right">
                    {deleteButton}
                    <IconButton icon="ok" text="Сохранить" type="success" handler={this.props.onSave} />
                </div>

                <div className="clearfix"></div>
                <hr className="content-header-hr" />
            </div>
        );
    }
}