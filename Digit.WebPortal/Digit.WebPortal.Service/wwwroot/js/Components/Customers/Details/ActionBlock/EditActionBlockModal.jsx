class EditActionBlockModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dueDate: null,
            message: null
        };

        this.inputChange = this.inputChange.bind(this);
        this.saveClick = this.saveClick.bind(this);
    }

    inputChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    saveClick() {
        if (!this.state.dueDate || !this.state.message) {
            return;
        }

        let dto = {
            message: this.state.message
        }

        let dueDate = moment.utc(this.state.dueDate, 'DD.MM.YYYY', true);
        if (!dueDate.isValid()) {
            alert("Invalid Date format");
            return;
        }

        dto.dueDate = dueDate.toJSON();

        CustomersApi.setAction(this.props.customer.id, dto, function () {
            location.reload();
        });
    }

    componentDidMount() {
        if (this.props.customer.action) {
            this.setState({
                dueDate: this.props.customer.action.dueDate,
                message: this.props.customer.action.message
            });
        }
    }

    render() {
        return (
            <div>
                <div id="editActionModal" className="modal fade" role="dialog">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal">&times; </button>
                                <h4 className="modal-title">
                                    <span className="glyphicon glyphicon-flash"></span> Напоминание
                                </h4>
                            </div>
                            <div className="modal-body">
                                <form className="form-horizontal">
                                    <div className="form-group">
                                        <label className="col-sm-2 control-label">Дата</label>
                                        <div className="col-sm-4">
                                            <input name="dueDate" className="form-control" type="text" placeholder="25.09.2017" value={this.state.dueDate} onChange={this.inputChange} />
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label className="col-sm-2 control-label">Сообщение</label>
                                        <div className="col-sm-8">
                                            <input name="message" className="form-control" type="text" value={this.state.message} onChange={this.inputChange} />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-default" data-dismiss="modal">Закрыть</button>
                                <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={this.saveClick}>Сохранить</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}