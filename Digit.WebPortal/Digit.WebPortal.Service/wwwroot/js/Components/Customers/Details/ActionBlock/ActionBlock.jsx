class ActionBlock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dueDate: null,
            message: null,
            overdue: null
        };

        this.deleteClick = this.deleteClick.bind(this);
    }

    deleteClick() {
        let self = this;
        CustomersApi.deleteAction(this.props.customer.id, function () {
            self.setState({
                dueDate: null,
                message: null,
                overdue: null
            });
        });
    }

    componentDidMount() {
        if (this.props.customer.action) {
            this.setState({
                dueDate: this.props.customer.action.dueDate,
                message: this.props.customer.action.message,
                overdue: this.props.customer.action.overdue,
            });
        }
    }

    render() {
        if (!this.state.message) {
            return <div />;
        }

        let labelType = this.state.overdue ? "label-danger" : "label-info";
        return (
            <div className="alert alert-warning" role="alert">
                <button type="button" className="close" aria-label="Close" onClick={this.deleteClick}>
                    <span aria-hidden="true">&times; </span>
                </button>
                <p>
                    <span className={"label " + labelType + " action-date-label"}>{this.state.dueDate}</span>
                    <span>{this.state.message}</span>
                </p>
            </div>
        );
    }
}