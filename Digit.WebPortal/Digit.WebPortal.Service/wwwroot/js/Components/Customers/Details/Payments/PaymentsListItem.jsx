class PaymentsListItem extends React.Component {
    constructor(props) {
        super(props);
        this.deleteClick = this.deleteClick.bind(this);
    }

    deleteClick() {
        let self = this;
        PaymentsApi.delete(this.props.payment.id, function () {
            self.props.handler();
        });
    }

    render() {
        return (
            <tr>
                <td>{this.props.payment.created}</td>
                <td>{this.props.payment.amount}</td>
                <td>{this.props.payment.periodStart}</td>
                <td>{this.props.payment.periodEnd}</td>
                <td>{this.props.payment.daysCount}</td>
                <td className="text-right">
                    <IconButton icon="trash" size="xs" type="danger" handler={this.deleteClick} />
                </td>
            </tr>
        );
    }
}