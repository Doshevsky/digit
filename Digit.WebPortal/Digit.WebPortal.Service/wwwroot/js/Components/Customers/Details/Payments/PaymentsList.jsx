class PaymentsList extends React.Component {
    render() {
        let self = this;
        return (
            <div id="payments" className="tab-pane fade customer-details-tab-block">
                <div className="table-responsive">
                    <table className="table table-hover">
                        <thead>
                            <tr>
                                <th>Дата</th>
                                <th>&#8381; </th>
                                <th>Начало</th>
                                <th>Конец</th>
                                <th>Дни</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody> {
                            this.props.payments.map(function (payment) {
                                return <PaymentsListItem payment={payment} handler={self.props.handler} />;
                            })
                        }
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}