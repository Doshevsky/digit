class AddPaymentModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            created: null,
            periodStart: null,
            daysCount: null,
            amount: null
        };

        this.inputChange = this.inputChange.bind(this);
        this.saveClick = this.saveClick.bind(this);
    }

    inputChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    saveClick() {
        let payment = new Payment();

        payment.created = moment.utc(this.state.created, 'DD.MM.YYYY', true);
        payment.periodStart = moment.utc(this.state.periodStart, 'DD.MM.YYYY', true);
        if (!payment.created.isValid() || !payment.periodStart.isValid()) {
            return;
        }

        payment.periodEnd = moment.utc(payment.periodStart).add(this.state.daysCount, 'days');
        if (payment.periodEnd <= payment.periodStart) {
            return;
        }

        payment.amount = this.state.amount;
        if (!(payment.amount > 0)) {
            return;
        }

        payment.created = payment.created.toJSON();
        payment.periodStart = payment.periodStart.toJSON();
        payment.periodEnd = payment.periodEnd.toJSON();

        let self = this;
        CustomersApi.addPayment(this.props.customerId, payment, function () {
            self.props.handler();
        });
    }

    render() {
        return (
            <div>
                <div id="addPaymentModal" className="modal fade" role="dialog">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal">&times; </button>
                                <h4 className="modal-title">Оплата</h4>
                            </div>
                            <div className="modal-body">
                                <form className="form-horizontal">
                                    <div className="form-group">
                                        <label className="col-sm-2 control-label">Дата</label>
                                        <div className="col-sm-6">
                                            <input name="created" className="form-control" type="text" placeholder="25.09.2017"
                                                value={this.state.created} onChange={this.inputChange} />
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <label className="col-sm-2 control-label">Начало</label>
                                        <div className="col-sm-6">
                                            <input name="periodStart" className="form-control" type="text" placeholder="25.09.2017"
                                                value={this.state.periodStart} onChange={this.inputChange} />
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <label className="col-sm-2 control-label">Дни</label>
                                        <div className="col-sm-2">
                                            <input name="daysCount" className="form-control" type="text" placeholder="30"
                                                value={this.state.daysCount} onChange={this.inputChange} />
                                        </div>
                                    </div>

                                    <div className="form-group">
                                        <label className="col-sm-2 control-label">Сумма</label>
                                        <div className="col-sm-4">
                                            <input name="amount" className="form-control" type="text" placeholder="9000"
                                                value={this.state.amount} onChange={this.inputChange} />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-default" data-dismiss="modal">Закрыть</button>
                                <button id="saveButton" type="button" className="btn btn-primary"
                                    data-dismiss="modal" onClick={this.saveClick}>Сохранить</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}