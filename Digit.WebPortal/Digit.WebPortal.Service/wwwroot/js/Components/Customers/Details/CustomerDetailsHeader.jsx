class CustomerDetailsHeader extends React.Component {
    constructor(props) {
        super(props);
        this.actionClick = this.actionClick.bind(this);
        this.deleteClick = this.deleteClick.bind(this);
    }

    actionClick() {
        $('#editActionModal').modal('show');
    }

    deleteClick() {
        $('#deleteModal').modal('show');
    }

    render() {
        let editPath = "/customers/edit/id" + this.props.customer.id;
        return (
            <div>
                <div className="content-header-left">
                    <h3 className="content-header-text">
                        {this.props.customer.fullName}, {this.props.customer.age}&nbsp;лет
                        <CustomerDetailsHeaderStatus customer={this.props.customer} />
                    </h3>
                </div>

                <div className="content-header-right">
                    <IconButton icon="flash" type="primary" handler={this.actionClick} />
                    <IconButton href={editPath} icon="pencil" type="primary" />
                    <IconButton icon="trash" type="danger" handler={this.deleteClick} />
                </div>

                <div className="clearfix"></div>
                <hr className="content-header-hr" />

                <EditActionBlockModal customer={this.props.customer} />
                <CustomerDetailsDeleteModal customer={this.props.customer} />
            </div>
        );
    }
}