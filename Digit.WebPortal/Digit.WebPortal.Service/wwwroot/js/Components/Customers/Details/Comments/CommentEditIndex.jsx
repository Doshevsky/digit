class CommentEditIndex extends React.Component {
    constructor(props) {
        super(props);
        this.state = { comment: null };

        this.messageChange = this.messageChange.bind(this);
        this.saveClick = this.saveClick.bind(this);
        this.deleteClick = this.deleteClick.bind(this);
    }

    messageChange(event) {
        let comment = this.state.comment;
        comment.message = event.target.value;
        this.setState({ comment: comment });

        if (!comment.message) {
            $('#saveButton').prop('disabled', true);
        }
        else {
            $('#saveButton').prop('disabled', false);
        }
    }

    saveClick() {
        let comment = this.state.comment;
        if (!comment.message) {
            return;
        }

        comment.created = undefined;

        CommentsApi.put(this.state.comment.id, this.state.comment, function () {
            location.replace(document.referrer);
        });
    }

    deleteClick() {
        CommentsApi.delete(this.state.comment.id, function () {
            location.replace(document.referrer);
        });
    }

    componentDidMount() {
        let self = this;
        CommentsApi.getById(self.props.params.id, function (comment) {
            self.setState({ comment: comment });
        });
    }

    render() {
        if (!this.state.comment) {
            return <p>Loading...</p>;
        }

        return (
            <div>
                <div className="content-header-left">
                    <h3 className="content-header-text">
                        Комментарий #{this.state.comment.id}
                    </h3>
                </div>

                <div className="content-header-right">
                    <IconButton icon="trash" text="Удалить" type="danger" handler={this.deleteClick} />
                    <IconButton icon="ok" text="Сохранить" type="success" handler={this.saveClick} />
                </div>

                <div className="clearfix"></div>
                <hr className="content-header-hr" />

                <textarea className="form-control" rows="3" value={this.state.comment.message} onChange={this.messageChange} />
            </div>
        );
    }
}