class CommentsListItem extends React.Component {
    render() {
        let editPath = "/customers/comments/edit/id" + this.props.comment.id;
        return (
            <div className="customer-details-comment-item">
                <p>{this.props.comment.message}</p>

                <div className="block-right">
                    <IconButton href={editPath} icon="pencil" size="xs" type="primary" />
                </div>

                <p className="text-muted">
                    {this.props.comment.author}, {this.props.comment.created}
                </p>
                <hr />
            </div>
        );
    }
}