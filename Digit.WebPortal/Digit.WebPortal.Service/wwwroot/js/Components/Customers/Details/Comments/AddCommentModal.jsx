class AddCommentModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = { comment: new Comment() };

        this.messageChange = this.messageChange.bind(this);
        this.saveClick = this.saveClick.bind(this);
    }

    messageChange(event) {
        let comment = this.state.comment;
        comment.message = event.target.value;
        this.setState({ comment: comment });
    }

    saveClick() {
        if (!this.state.comment.message) {
            return;
        }

        let self = this;
        CustomersApi.addComment(this.props.customerId, this.state.comment, function() {
            self.props.handler();
        });
    }

    render() {
        return (
            <div>
                <div id="addCommentModal" className="modal fade" role="dialog">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal">&times; </button>
                                <h4 className="modal-title">Комментарий</h4>
                            </div>
                            <div className="modal-body">
                                <textarea className="form-control" rows="3" value={this.state.comment.message} onChange={this.messageChange} />
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-default" data-dismiss="modal">Закрыть</button>
                                <button id="saveButton" type="button" className="btn btn-primary"
                                    data-dismiss="modal" onClick={this.saveClick}>Сохранить</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}