class CommentsList extends React.Component {
    render() {
        return (
            <div id="comments" className="tab-pane fade in active customer-details-tab-block"> {
                this.props.comments.map(function (comment) {
                    return <CommentsListItem comment={comment} />;
                })
            }
            </div>
        );
    }
}