class CustomerDetailsTabs extends React.Component {
    constructor(props) {
        super(props);
        this.addClick = this.addClick.bind(this);
    }

    addClick() {
        let tab = $("ul#tabs li.active")[0];
        switch (tab.id) {
            case '1':
                $('#addCommentModal').modal('show');
                break;
            case '2':
                $('#addVisitModal').modal('show');
                break;
            case '3':
                $('#addPaymentModal').modal('show');
                break;
        }
    }

    render() {
        return (
            <div>
                <ul id="tabs" className="nav nav-pills">
                    <li id="1" className="active"><a data-toggle="pill" href="#comments">Комментарии</a></li>
                    <li id="2"><a data-toggle="pill" href="#visits">Посещения</a></li>
                    <li id="3"><a data-toggle="pill" href="#payments">Оплата</a></li>

                    <div className="block-right">
                        <IconButton icon="plus" size="sm" type="primary" handler={this.addClick} />
                    </div>
                </ul>

                <div className="tab-content">
                    <CommentsList comments={this.props.customer.comments} />
                    <VisitsList visits={this.props.customer.visits} handler={this.props.handler} />
                    <PaymentsList payments={this.props.customer.payments} handler={this.props.handler} />
                </div>

                <AddCommentModal customerId={this.props.customer.id} handler={this.props.handler} />
                <AddVisitModal customerId={this.props.customer.id} handler={this.props.handler} />
                <AddPaymentModal customerId={this.props.customer.id} handler={this.props.handler} />
            </div>
        );
    }
}