class CustomerEditContactItemForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = { contact: this.props.contact };

        this.typeSelected = this.typeSelected.bind(this);
        this.valueChanged = this.valueChanged.bind(this);
    }

    typeSelected(input) {
        let option = input.target[input.target.selectedIndex];
        let contact = this.state.contact;
        contact.type = option.value;
        contact.title = Contact.types[option.value];
        this.setState({ contact: contact });
    }

    valueChanged(input) {
        let contact = this.state.contact;
        contact.value = input.target.value;
        this.setState({ contact: contact });
    }

    render() {
        return (
            <div>
                <form className="form-inline customer-edit-contact-item">
                    <div className="form-group">
                        <div>
                            <select className="form-control" value={this.state.contact.type} onChange={this.typeSelected}> {
                                Contact.types.map(function (title, index) {
                                    return <option value={index}>{title}</option>
                                })}
                            </select>
                        </div>
                    </div>

                    <div className="form-group">
                        <div>
                            <input className="form-control" type="text" placeholder="Значение" value={this.state.contact.value} onChange={this.valueChanged} />
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}