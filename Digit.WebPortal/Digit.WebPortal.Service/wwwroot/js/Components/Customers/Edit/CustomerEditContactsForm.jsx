class CustomerEditContactsForm extends React.Component {
    constructor(props) {
        super(props);

        let contacts = this.props.contacts;
        if (!contacts || !contacts.length) {
            contacts = [new Contact({ type: 2 })];
        }

        this.state = { contacts: contacts };
        this.addContact = this.addContact.bind(this);
    }

    addContact() {
        this.setState({ contacts: this.state.contacts.concat([new Contact({ type: 2 })]) });
    }

    render() {
        return (
            <div className="customer-edit-contacts-form">
                <CustomerEditContactsFormHeader addContactHandler={this.addContact} /> {
                    this.state.contacts.map(function(contact) {
                        return <CustomerEditContactItemForm contact={contact} />
                    })
                }
            </div>
        );
    }
}