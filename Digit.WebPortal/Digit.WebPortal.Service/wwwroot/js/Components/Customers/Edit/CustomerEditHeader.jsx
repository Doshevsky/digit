class CustomerEditHeader extends React.Component {
    render() {
        let title = this.props.customer.id ? 'Клиент #' + this.props.customer.id : 'Новый клиент';
        return (
            <div>
                <div className="content-header-left">
                    <h3 className="content-header-text">
                        {title}
                    </h3>
                </div>

                <div className="content-header-right">
                    <IconButton icon="ok" text="Сохранить" type="success" handler={this.props.onSave} />
                </div>

                <div className="clearfix"></div>
                <hr className="content-header-hr" />
            </div>
        );
    }
}