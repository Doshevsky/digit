class CustomerEditContactsFormHeader extends React.Component {
    render() {
        return (
            <div>
                <div className="content-header-left">
                    <h4 className="content-header-text">Контакты</h4>
                </div>

                <div className="content-header-right">
                    <IconButton icon="plus" type="primary" size="sm" handler={this.props.addContactHandler} />
                </div>

                <div className="clearfix"></div>
                <hr className="content-header-hr" />
            </div>
        );
    }
}