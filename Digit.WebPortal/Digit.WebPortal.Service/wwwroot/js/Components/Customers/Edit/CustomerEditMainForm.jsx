class CustomerEditMainForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: this.props.customer.firstName,
            lastName: this.props.customer.lastName,
            dateOfBirth: this.formatDateOfBirth(this.props.customer.dateOfBirth),
            channel: this.props.customer.channel,
            information: this.props.customer.information,
        };

        this.inputChange = this.inputChange.bind(this);
        this.channelChange = this.channelChange.bind(this);
    }

    formatDateOfBirth(value) {
        if (!value) {
            return null;
        }

        let date = moment.utc(value);
        return date.format('DD.MM.YYYY');
    }

    inputChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    channelChange(value) {
        let channel = new Channel(value);
        this.setState({ channel: channel });
    }

    render() {
        return (
            <div>
                <form className="form-horizontal">
                    <div className="form-group">
                        <label className="col-sm-2 control-label">Фамилия</label>
                        <div className="col-sm-8">
                            <input name="lastName" className="form-control" type="text" value={this.state.lastName} onChange={this.inputChange} />
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="col-sm-2 control-label">Имя</label>
                        <div className="col-sm-8">
                            <input name="firstName" className="form-control" type="text" value={this.state.firstName} onChange={this.inputChange} />
                        </div>
                    </div>

                    <div className="form-group">
                        <label className="col-sm-2 control-label">Дата рождения</label>
                        <div className="col-sm-2">
                            <input name="dateOfBirth" className="form-control" type="text" value={this.state.dateOfBirth} onChange={this.inputChange} placeholder="12.03.2000" />
                        </div>
                    </div>

                    <CustomerEditChannelForm channel={this.state.channel} onChange={this.channelChange} />

                    <div className="form-group">
                        <label className="col-sm-2 control-label">Информация</label>
                        <div className="col-sm-8">
                            <textarea name="information" className="form-control" rows="3" value={this.state.information} onChange={this.inputChange}></textarea>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}