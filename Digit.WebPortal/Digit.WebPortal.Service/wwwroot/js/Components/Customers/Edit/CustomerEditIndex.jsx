class CustomerEditIndex extends React.Component {
    constructor(props) {
        super(props);
        this.state = { customer: null };

        this.save = this.save.bind(this);
    }

    componentDidMount() {
        let customerId = this.props.params.id;
        if (customerId) {
            let self = this;
            CustomersApi.getById(customerId, function (data) {
                self.setState({ customer: data });
            });
        }
        else {
            this.setState({ customer: new Customer() });
        }
    }

    save() {
        let dateOfBirth = null;
        let dateOfBirthValue = this.refs.mainForm.state.dateOfBirth;
        if (dateOfBirthValue) {
            dateOfBirth = moment.utc(dateOfBirthValue, 'DD.MM.YYYY', true);
            if (dateOfBirth.isValid()) {
                dateOfBirth = dateOfBirth.toJSON();
            }
            else {
                alert('Invalid date of birth format');
                return;
            }
        }

        let contacts = [];
        this.refs.contactsForm.state.contacts.map(function (contact) {
            if (contact.value) {
                contacts.push({
                    type: contact.type,
                    value: contact.value
                });
            }
        });

        let customer = this.state.customer;
        customer.firstName = this.refs.mainForm.state.firstName;
        customer.lastName = this.refs.mainForm.state.lastName;
        customer.dateOfBirth = dateOfBirth;
        customer.information = this.refs.mainForm.state.information;
        customer.channel = this.refs.mainForm.state.channel ? this.refs.mainForm.state.channel.id : 0;
        customer.contacts = contacts;

        if (customer.id != undefined) {
            customer.status = customer.status.id;
            CustomersApi.put(customer.id, customer, function () {
                window.location.replace('/customers/id' + customer.id);
            })
        }
        else {
            CustomersApi.post(customer, function (result) {
                window.location.replace('/customers/id' + result.id);
            })
        }
    }

    render() {
        if (!this.state.customer) {
            return <p>Loading...</p>;
        }

        return (
            <div>
                <div className="row">
                    <CustomerEditHeader customer={this.state.customer} onSave={this.save} />
                </div>

                <div className="row">
                    <CustomerEditMainForm customer={this.state.customer} ref="mainForm" />
                    <CustomerEditContactsForm contacts={this.state.customer.contacts} ref="contactsForm" />
                </div>
            </div>
        );
    }
}