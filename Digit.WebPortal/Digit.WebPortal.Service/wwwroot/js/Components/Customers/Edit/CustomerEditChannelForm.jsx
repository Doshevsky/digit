class CustomerEditChannelForm extends React.Component {
    constructor(props) {
        super(props);

        let channelId = this.props.channel ? this.props.channel.id : 0;
        this.state = { selectedChannelId: channelId };
        this.channelChange = this.channelChange.bind(this);
    }

    channelChange(input) {
        let option = input.target[input.target.selectedIndex];
        this.setState({ selectedChannelId: option.value });
        this.props.onChange(option.value);
    }

    render() {
        return (
            <div>
                <div className="form-group">
                    <label className="col-sm-2 control-label">Канал</label>
                    <div className="col-sm-2">
                        <select className="form-control" value={this.state.selectedChannelId} onChange={this.channelChange}>{
                            Object.keys(ChannelsList).map(function (channelId) {
                                return (
                                    <option value={channelId}>{ChannelsList[channelId]}</option>
                                );
                            })}
                        </select>
                    </div>
                </div>
            </div>
        );
    }
}