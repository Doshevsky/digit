class CustomersIndex extends React.Component {
    constructor(props) {
        super(props);
        this.state = { customers: undefined };

        this.loadData = this.loadData.bind(this);
        this.parametersUpdate = this.parametersUpdate.bind(this);
    }

    loadData(status, sort, searchQuery) {
        let self = this;
        CustomersApi.get(status, sort, searchQuery, function (data) {
            self.setState({ customers: data });
        });
    }

    parametersUpdate(status, sort, searchQuery) {
        this.loadData(status, sort, searchQuery);
    }

    componentDidMount() {
        this.loadData(null, null, null);
    }

    render() {
        return (
            <div>
                <div className="row">
                    <CustomersHeader onParametersUpdate={this.parametersUpdate} />
                </div>

                <div className="row">
                    <CustomersList customers={this.state.customers} />
                </div>
            </div>
        );
    }
}