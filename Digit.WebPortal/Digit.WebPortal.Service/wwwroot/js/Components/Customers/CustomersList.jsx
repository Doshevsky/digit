class CustomersList extends React.Component {
    render() {
        if (!this.props.customers) {
            return <p>Loading...</p>;
        }

        return (
            <div className="table-responsive">
                <table className="table table-hover">
                    <thead>
                        <tr>
                            <th>Имя</th>
                            <th>Возраст</th>
                            <th>Статус</th>
                            <th>Напоминание</th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody> {
                        this.props.customers.map(function (customer) {
                            return <CustomersListItem customer={customer} />;
                        })
                    }
                    </tbody>
                </table>
            </div>
        );
    }
}