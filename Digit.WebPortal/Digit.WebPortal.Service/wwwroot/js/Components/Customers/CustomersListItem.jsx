class CustomersListItem extends React.Component {
    render() {
        let detailsPath = "/customers/id" + this.props.customer.id;

        let action = <p></p>;
        if (this.props.customer.action) {
            let labelType = this.props.customer.action.overdue ? "label-danger" : "label-info";
            action = (
                <p>
                    <span className={"label " + labelType + " action-date-label"}>{this.props.customer.action.dueDate}</span>
                    <span>{this.props.customer.action.message}</span>
                </p>
            )
        }

        let visitAlert = <p></p>;
        if (this.props.customer.notAttend) {
            visitAlert = (
                <span className="label label-warning">
                    <span className="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                </span>
            );
        }

        let paymentAlert = <p></p>;
        if (this.props.customer.paymentSoonExpire) {
            paymentAlert = (
                <span className="label label-warning">
                    <span className="glyphicon glyphicon-ruble" aria-hidden="true"></span>
                </span>
            );
        }
        else if (this.props.customer.paymentExpired) {
            paymentAlert = (
                <span className="label label-danger">
                    <span className="glyphicon glyphicon-ruble" aria-hidden="true"></span>
                </span>
            );
        }

        return (
            <tr>
                <td>{this.props.customer.fullName}</td>
                <td>{this.props.customer.age}</td>
                <td>{this.props.customer.status.title}</td>
                <td>{action}</td>
                <td>{visitAlert}</td>
                <td>{paymentAlert}</td>
                <td className="text-right">
                    <IconButton href={detailsPath} icon="option-horizontal" size="xs" type="primary" />
                </td>
            </tr>
        );
    }
}