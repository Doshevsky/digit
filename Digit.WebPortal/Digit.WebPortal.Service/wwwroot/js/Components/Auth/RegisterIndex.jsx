class RegisterIndex extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: null,
            email: null,
            password: null,
            validatePassword: null
        };

        this.inputChange = this.inputChange.bind(this);
        this.registerClick = this.registerClick.bind(this);
    }

    inputChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    registerClick() {
        if (this.state.password != this.state.validatePassword) {
            alert("Пароли не совпадают");
            return;
        }

        let dto = {
            username: this.state.username,
            email: this.state.email,
            password: this.state.password,
        }

        AuthApi.register(dto, function () {
            window.location.replace('/home');
        });
    }

    render() {
        return (
            <div>
                <div className="col-md-4"></div>
                <div className="col-md-4">
                    <h2>Регистрация</h2>

                    <form>
                        <div className="form-group">
                            <label>Имя пользователя</label>
                            <input name="username" type="text" className="form-control"
                                value={this.state.username} onChange={this.inputChange}/>
                        </div>
                        <div className="form-group">
                            <label>Email</label>
                            <input name="email" type="text" className="form-control"
                                value={this.state.email} onChange={this.inputChange}/>
                        </div>
                        <div className="form-group">
                            <label>Пароль</label>
                            <input name="password" type="password" className="form-control"
                                value={this.state.password} onChange={this.inputChange}/>
                        </div>
                        <div className="form-group">
                            <label>Повторите пароль</label>
                            <input name="validatePassword" type="password" className="form-control"
                                value={this.state.validatePassword} onChange={this.inputChange}/>
                        </div>
                        <div className="form-group">
                            <input value="Регистрация" className="btn btn-primary" onClick={this.registerClick}/>
                        </div>
                    </form>

                    <a href="/login">Вход</a>
                </div>
            </div>
        );
    }
}