class LoginIndex extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: null,
            password: null
        };

        this.inputChange = this.inputChange.bind(this);
        this.loginClick = this.loginClick.bind(this);
    }

    inputChange(event) {
        this.setState({ [event.target.name]: event.target.value });
    }

    loginClick() {
        let dto = {
            username: this.state.username,
            password: this.state.password
        }

        AuthApi.login(dto, function () {
            window.location.replace('/home');
        });
    }

    render() {
        return (
            <div>
                <div className="col-md-4"></div>
                <div className="col-md-4">
                    <h2>Вход</h2>

                    <form>
                        <div className="form-group">
                            <label>Имя пользователя</label>
                            <input name="username" type="text" className="form-control"
                                value={this.state.username} onChange={this.inputChange}/>
                        </div>
                        <div className="form-group">
                            <label>Пароль</label>
                            <input name="password" type="password" className="form-control"
                                value={this.state.password} onChange={this.inputChange}/>
                        </div>
                        <div className="form-group">
                            <input value="Войти" className="btn btn-primary" onClick={this.loginClick} />
                        </div>
                    </form>

                    <a href="/register">Регистрация</a>
                </div>
            </div>
        );
    }
}