class User {
    constructor(model) {
        this.id = model.id;
        this.email = model.email;
        this.username = model.username;
        this.role = new Role(model.role);
    }
}