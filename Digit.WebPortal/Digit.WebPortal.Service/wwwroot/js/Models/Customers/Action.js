class Action {
    constructor(model) {
        this.dueDate = moment(model.dueDate).format("DD.MM.YYYY");
        this.message = model.message;
        this.overdue = model.overdue;
    }
}