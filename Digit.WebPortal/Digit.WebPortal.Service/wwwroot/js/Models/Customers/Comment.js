class Comment {
    constructor(model) {
        if (model) {
            this.id = model.id;
            this.message = model.message;
            this.created = moment(model.created).format("DD.MM.YYYY HH:mm");
            this.author = model.author;
        }
    }
}