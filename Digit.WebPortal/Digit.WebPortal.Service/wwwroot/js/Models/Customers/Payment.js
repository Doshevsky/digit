class Payment {
    constructor(model) {
        if (model) {
            this.id = model.id;
            this.created = moment.utc(model.created).format("DD.MM.YYYY");
            this.periodStart = moment.utc(model.periodStart).format("DD.MM.YYYY");
            this.periodEnd = moment.utc(model.periodEnd).format("DD.MM.YYYY");
            this.amount = model.amount;
            this.daysCount = this.calculateDaysCount(model);
        }
    }

    calculateDaysCount(model) {
        let start = moment(model.periodStart);
        let end = moment(model.periodEnd);
        let duration = moment.duration(end.diff(start));
        return duration.asDays();
    }
}