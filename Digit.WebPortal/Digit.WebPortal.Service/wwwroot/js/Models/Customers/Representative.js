class Representative {
    constructor(model) {
        if (model) {
            this.id = model.id;
            this.firstName = model.firstName;
            this.middleName = model.middleName;
            this.lastName = model.lastName;
            this.fullName = this.getFullName(model);
            this.comment = model.comment;
            this.contacts = this.mapContacts(model);
        }
    }

    getFullName(model) {
        let firstName = model.firstName ? model.firstName : '-';
        let middleName = model.middleName ? model.middleName : '-';
        let lastName = model.lastName ? model.lastName : '-';
        return lastName + " " + firstName + " " + middleName;
    }

    mapContacts(model) {
        return model.contacts.map(function(contact) {
            return new Contact(contact);
        })
    }
}