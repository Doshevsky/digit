class Customer {
    constructor(model) {
        if (model) {
            this.id = model.id;
            this.firstName = model.firstName;
            this.lastName = model.lastName;
            this.fullName = this.getFullName(model);
            this.dateOfBirth = model.dateOfBirth;
            this.age = model.age;
            this.status = new Status(model.status);
            this.channel = new Channel(model.channel);
            this.information = model.information;
            this.action = model.action ? new Action(model.action) : null;
            this.contacts = this.mapContacts(model);
            this.representatives = this.mapRepresentatives(model);
            this.comments = this.mapComments(model);
            this.visits = this.mapVisits(model);
            this.payments = this.mapPayments(model);
            this.paymentSoonExpire = model.paymentSoonExpire;
            this.paymentExpired = model.paymentExpired;
            this.notAttend = model.notAttend;
        }
    }

    getFullName(model) {
        let firstName = model.firstName ? model.firstName : '-';
        let lastName = model.lastName ? model.lastName : '-'; 
        return lastName + ' ' + firstName;
    }

    mapContacts(model) {
        return model.contacts.map(function (contact) {
            return new Contact(contact);
        })
    }

    mapRepresentatives(model) {
        return model.representatives.map(function (representative) {
            return new Representative(representative);
        })
    }

    mapComments(model) {
        return model.comments.map(function (comment) {
            return new Comment(comment);
        })
    }

    mapVisits(model) {
        return model.visits.map(function (visit) {
            return new Visit(visit);
        })
    }

    mapPayments(model) {
        return model.payments.map(function (payment) {
            return new Payment(payment);
        })
    }
}