class Visit {
    constructor(model) {
        if (model) {
            this.id = model.id;
            this.date = null;
            this.start = null;
            this.end = null;
            this.duration = null;

            this.initializeDates(model);
        }
    }

    initializeDates(model) {
        let start = moment.utc(model.start);
        let end = moment.utc(model.end);

        this.date = start.format('DD.MM.YYYY');
        this.start = this.getTime(start);

        if (end.isValid()) {
            this.end = this.getTime(end);

            let duration = moment.duration(end.diff(start)).asHours();
            this.duration = duration ? duration : null;
        }
    }

    getTime(date) {
        if (date.minute() || date.second()) {
            return date.format('HH:mm');
        }
        else {
            return null;
        }
    }
}