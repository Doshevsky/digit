class Contact {
    constructor(model) {
        this.id = model.id;
        this.type = model.type;
        this.title = Contact.types[model.type];
        this.value = model.value;
    }
}

Contact.types = ['-', 'Адрес', 'Телефон', 'Email', 'Школа'];