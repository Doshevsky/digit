﻿class App extends React.Component {
    render() {
        let path = this.props.location.pathname;
        if (path == '/login' || path == '/register') {
            return this.props.children;
        }

        return (
            <div>
                <Navbar />
                <MainMenu />

                <div className="container main-content">
                    {this.props.children}
                </div>
            </div>
        );
    }
}

ReactDOM.render((
    <ReactRouter.Router history={ReactRouter.browserHistory}>
        <ReactRouter.Route path="/" component={App}>
            <ReactRouter.IndexRoute component={HomeIndex} />
            <ReactRouter.Route path="login" component={LoginIndex} />
            <ReactRouter.Route path="register" component={RegisterIndex} />
            <ReactRouter.Route path="home" component={HomeIndex} />
            <ReactRouter.Route path="users" component={UsersIndex} />
            <ReactRouter.Route path="users/id:id" component={UserDetailsIndex} />
            <ReactRouter.Route path="customers" component={CustomersIndex} />
            <ReactRouter.Route path="customers/id:id" component={CustomerDetailsIndex} />
            <ReactRouter.Route path="customers/edit" component={CustomerEditIndex} />
            <ReactRouter.Route path="customers/edit/id:id" component={CustomerEditIndex} />
            <ReactRouter.Route path="customers/comments/edit/id:id" component={CommentEditIndex} />
            <ReactRouter.Route path="customers/id:customerid/representatives/edit" component={RepresentativeEditIndex} />
            <ReactRouter.Route path="customers/id:customerid/representatives/edit/id:id" component={RepresentativeEditIndex} />
            <ReactRouter.Route path="stat/visits" component={StatVisits} />
        </ReactRouter.Route>
    </ReactRouter.Router>
), document.getElementById('content'));