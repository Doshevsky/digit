﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Digit.WebPortal.Service.Data;

namespace Digit.WebPortal.Service.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20170912184714_v5")]
    partial class v5
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Digit.WebPortal.CustomerManagement.Data.CommentData", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AuthorId");

                    b.Property<DateTime>("Created");

                    b.Property<int>("CustomerId");

                    b.Property<string>("Message");

                    b.HasKey("Id");

                    b.HasIndex("CustomerId");

                    b.ToTable("Comments");
                });

            modelBuilder.Entity("Digit.WebPortal.CustomerManagement.Data.ContactData", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("CustomerId");

                    b.Property<int?>("RepresentativeId");

                    b.Property<int>("Type");

                    b.Property<string>("Value");

                    b.HasKey("Id");

                    b.HasIndex("CustomerId");

                    b.HasIndex("RepresentativeId");

                    b.ToTable("Contacts");
                });

            modelBuilder.Entity("Digit.WebPortal.CustomerManagement.Data.CustomerData", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Action");

                    b.Property<int>("Channel");

                    b.Property<DateTime>("Created");

                    b.Property<DateTime?>("DateOfBirth");

                    b.Property<string>("FirstName");

                    b.Property<string>("Information");

                    b.Property<string>("LastName");

                    b.Property<int>("Status");

                    b.HasKey("Id");

                    b.ToTable("Customers");
                });

            modelBuilder.Entity("Digit.WebPortal.CustomerManagement.Data.PaymentData", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Amount");

                    b.Property<DateTime>("Created");

                    b.Property<int?>("CustomerId");

                    b.Property<DateTime>("PeriodEnd");

                    b.Property<DateTime>("PeriodStart");

                    b.HasKey("Id");

                    b.HasIndex("CustomerId");

                    b.ToTable("Payments");
                });

            modelBuilder.Entity("Digit.WebPortal.CustomerManagement.Data.RepresentativeData", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Comment");

                    b.Property<int?>("CustomerId");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<string>("MiddleName");

                    b.HasKey("Id");

                    b.HasIndex("CustomerId");

                    b.ToTable("Representatives");
                });

            modelBuilder.Entity("Digit.WebPortal.CustomerManagement.Data.VisitData", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("CustomerId");

                    b.Property<DateTime?>("End");

                    b.Property<DateTime>("Start");

                    b.HasKey("Id");

                    b.HasIndex("CustomerId");

                    b.ToTable("Visits");
                });

            modelBuilder.Entity("Digit.WebPortal.UserManagement.Data.UserData", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Email");

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PasswordSalt");

                    b.Property<int>("Role");

                    b.Property<string>("Username");

                    b.HasKey("Id");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("Digit.WebPortal.CustomerManagement.Data.CommentData", b =>
                {
                    b.HasOne("Digit.WebPortal.CustomerManagement.Data.CustomerData")
                        .WithMany("Comments")
                        .HasForeignKey("CustomerId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Digit.WebPortal.CustomerManagement.Data.ContactData", b =>
                {
                    b.HasOne("Digit.WebPortal.CustomerManagement.Data.CustomerData")
                        .WithMany("Contacts")
                        .HasForeignKey("CustomerId");

                    b.HasOne("Digit.WebPortal.CustomerManagement.Data.RepresentativeData")
                        .WithMany("Contacts")
                        .HasForeignKey("RepresentativeId");
                });

            modelBuilder.Entity("Digit.WebPortal.CustomerManagement.Data.PaymentData", b =>
                {
                    b.HasOne("Digit.WebPortal.CustomerManagement.Data.CustomerData")
                        .WithMany("Payments")
                        .HasForeignKey("CustomerId");
                });

            modelBuilder.Entity("Digit.WebPortal.CustomerManagement.Data.RepresentativeData", b =>
                {
                    b.HasOne("Digit.WebPortal.CustomerManagement.Data.CustomerData")
                        .WithMany("Representatives")
                        .HasForeignKey("CustomerId");
                });

            modelBuilder.Entity("Digit.WebPortal.CustomerManagement.Data.VisitData", b =>
                {
                    b.HasOne("Digit.WebPortal.CustomerManagement.Data.CustomerData")
                        .WithMany("Visits")
                        .HasForeignKey("CustomerId");
                });
        }
    }
}
