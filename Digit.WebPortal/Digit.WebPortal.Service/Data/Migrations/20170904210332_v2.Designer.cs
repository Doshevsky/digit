﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Digit.WebPortal.Service.Data;

namespace Digit.WebPortal.Service.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20170904210332_v2")]
    partial class v2
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Digit.WebPortal.Models.Customers.Channel", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Title");

                    b.HasKey("Id");

                    b.ToTable("Channels");
                });

            modelBuilder.Entity("Digit.WebPortal.Models.Customers.Comment", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Author");

                    b.Property<DateTime>("Created");

                    b.Property<int?>("CustomerId");

                    b.Property<string>("Message");

                    b.HasKey("Id");

                    b.HasIndex("CustomerId");

                    b.ToTable("Comments");
                });

            modelBuilder.Entity("Digit.WebPortal.Models.Customers.Contact", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("CustomerId");

                    b.Property<int?>("RepresentativeId");

                    b.Property<int>("Type");

                    b.Property<string>("Value");

                    b.HasKey("Id");

                    b.HasIndex("CustomerId");

                    b.HasIndex("RepresentativeId");

                    b.ToTable("Contact");
                });

            modelBuilder.Entity("Digit.WebPortal.Models.Customers.Customer", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Action");

                    b.Property<int?>("ChannelId");

                    b.Property<DateTime>("Created");

                    b.Property<DateTime?>("DateOfBirth");

                    b.Property<string>("FirstName");

                    b.Property<string>("Information");

                    b.Property<string>("LastName");

                    b.Property<int?>("StatusId");

                    b.HasKey("Id");

                    b.HasIndex("ChannelId");

                    b.HasIndex("StatusId");

                    b.ToTable("Customers");
                });

            modelBuilder.Entity("Digit.WebPortal.Models.Customers.Payment", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("Amount");

                    b.Property<DateTime>("Created");

                    b.Property<int?>("CustomerId");

                    b.Property<DateTime>("PeriodEnd");

                    b.Property<DateTime>("PeriodStart");

                    b.HasKey("Id");

                    b.HasIndex("CustomerId");

                    b.ToTable("Payments");
                });

            modelBuilder.Entity("Digit.WebPortal.Models.Customers.Representative", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Comment");

                    b.Property<int?>("CustomerId");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<string>("MiddleName");

                    b.HasKey("Id");

                    b.HasIndex("CustomerId");

                    b.ToTable("Representatives");
                });

            modelBuilder.Entity("Digit.WebPortal.Models.Customers.Status", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Title");

                    b.HasKey("Id");

                    b.ToTable("Statuses");
                });

            modelBuilder.Entity("Digit.WebPortal.Models.Customers.Visit", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("CustomerId");

                    b.Property<DateTime?>("End");

                    b.Property<DateTime>("Start");

                    b.HasKey("Id");

                    b.HasIndex("CustomerId");

                    b.ToTable("Visits");
                });

            modelBuilder.Entity("Digit.WebPortal.UserManagement.Data.UserData", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Email");

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PasswordSalt");

                    b.Property<int>("Role");

                    b.Property<string>("Username");

                    b.HasKey("Id");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("Digit.WebPortal.Models.Customers.Comment", b =>
                {
                    b.HasOne("Digit.WebPortal.Models.Customers.Customer")
                        .WithMany("Comments")
                        .HasForeignKey("CustomerId");
                });

            modelBuilder.Entity("Digit.WebPortal.Models.Customers.Contact", b =>
                {
                    b.HasOne("Digit.WebPortal.Models.Customers.Customer")
                        .WithMany("Contacts")
                        .HasForeignKey("CustomerId");

                    b.HasOne("Digit.WebPortal.Models.Customers.Representative")
                        .WithMany("Contacts")
                        .HasForeignKey("RepresentativeId");
                });

            modelBuilder.Entity("Digit.WebPortal.Models.Customers.Customer", b =>
                {
                    b.HasOne("Digit.WebPortal.Models.Customers.Channel", "Channel")
                        .WithMany()
                        .HasForeignKey("ChannelId");

                    b.HasOne("Digit.WebPortal.Models.Customers.Status", "Status")
                        .WithMany()
                        .HasForeignKey("StatusId");
                });

            modelBuilder.Entity("Digit.WebPortal.Models.Customers.Payment", b =>
                {
                    b.HasOne("Digit.WebPortal.Models.Customers.Customer")
                        .WithMany("Payments")
                        .HasForeignKey("CustomerId");
                });

            modelBuilder.Entity("Digit.WebPortal.Models.Customers.Representative", b =>
                {
                    b.HasOne("Digit.WebPortal.Models.Customers.Customer")
                        .WithMany("Representatives")
                        .HasForeignKey("CustomerId");
                });

            modelBuilder.Entity("Digit.WebPortal.Models.Customers.Visit", b =>
                {
                    b.HasOne("Digit.WebPortal.Models.Customers.Customer")
                        .WithMany("Visits")
                        .HasForeignKey("CustomerId");
                });
        }
    }
}
