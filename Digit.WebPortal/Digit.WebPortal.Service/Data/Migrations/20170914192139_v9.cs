﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Digit.WebPortal.Service.Migrations
{
    public partial class v9 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Actions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CustomerId = table.Column<int>(nullable: false),
                    DueDate = table.Column<DateTime>(nullable: false),
                    Message = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Actions", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Actions_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Actions_CustomerId",
                table: "Actions",
                column: "CustomerId");

            //migrationBuilder.Sql(@"
            //    INSERT INTO Actions
            //    SELECT id,
            //        CONVERT(datetime, SUBSTRING(Action, 1, CHARINDEX(':', Action)-1), 104),
            //        LTRIM(SUBSTRING(Action, CHARINDEX(':', Action) + 1, 1000))
            //    FROM Customers
            //    WHERE Action IS NOT NULL");

            migrationBuilder.DropColumn(
                name: "Action",
                table: "Customers");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Actions");

            migrationBuilder.AddColumn<string>(
                name: "Action",
                table: "Customers",
                nullable: true);
        }
    }
}
