﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Digit.WebPortal.Service.Migrations
{
    public partial class v3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Channel",
                table: "Customers",
                nullable: false,
                defaultValue: 0);

            //migrationBuilder.Sql("UPDATE Customers SET Channel = '1' WHERE ChannelId = '4' OR ChannelId = '5'");
            //migrationBuilder.Sql("UPDATE Customers SET Channel = '2' WHERE ChannelId = '1' OR ChannelId = '2'");
            //migrationBuilder.Sql("UPDATE Customers SET Channel = '3' WHERE ChannelId = '3'");

            migrationBuilder.DropForeignKey(
                name: "FK_Customers_Channels_ChannelId",
                table: "Customers");

            migrationBuilder.DropTable(
                name: "Channels");

            migrationBuilder.DropIndex(
                name: "IX_Customers_ChannelId",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "ChannelId",
                table: "Customers");

            ///////////////////////////////////////

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "Customers",
                nullable: false,
                defaultValue: 0);

            //migrationBuilder.Sql("UPDATE Customers SET Status = '1' WHERE StatusId = '1'");
            //migrationBuilder.Sql("UPDATE Customers SET Status = '2' WHERE StatusId = '2'");
            //migrationBuilder.Sql("UPDATE Customers SET Status = '3' WHERE StatusId = '3'");
            //migrationBuilder.Sql("UPDATE Customers SET Status = '4' WHERE StatusId = '4'");

            migrationBuilder.DropForeignKey(
                name: "FK_Customers_Statuses_StatusId",
                table: "Customers");

            migrationBuilder.DropTable(
                name: "Statuses");

            migrationBuilder.DropIndex(
                name: "IX_Customers_StatusId",
                table: "Customers");

            migrationBuilder.DropColumn(
                name: "StatusId",
                table: "Customers");

            ///////////////////////////////////////

            migrationBuilder.AddColumn<int>(
                name: "AuthorId",
                table: "Comments",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.Sql("UPDATE Comments SET AuthorId = '1'");

            migrationBuilder.DropColumn(
                name: "Author",
                table: "Comments");

            ///////////////////////////////////////

            migrationBuilder.DropForeignKey(
                name: "FK_Representatives_Customers_CustomerId",
                table: "Representatives");

            migrationBuilder.RenameTable(
                name: "Contact",
                newName: "Contacts");

            migrationBuilder.AlterColumn<int>(
                name: "CustomerId",
                table: "Representatives",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Representatives_Customers_CustomerId",
                table: "Representatives",
                column: "CustomerId",
                principalTable: "Customers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            throw new NotSupportedException();
        }
    }
}
