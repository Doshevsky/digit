﻿using Digit.WebPortal.Core;
using Digit.WebPortal.CustomerManagement;
using Digit.WebPortal.CustomerManagement.Data;
using Digit.WebPortal.UserManagement;
using Digit.WebPortal.UserManagement.Data;
using Microsoft.EntityFrameworkCore;

namespace Digit.WebPortal.Service.Data
{
    public class AppDbContext : DbContext, IUserManagementDbContext, ICustomerManagementDbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options)
            : base(options)
        {
        }

        public DbSet<UserData> Users { get; set; }

        public DbSet<CustomerData> Customers { get; set; }
        public DbSet<ActionData> Actions { get; set; }
        public DbSet<RepresentativeData> Representatives { get; set; }
        public DbSet<ContactData> Contacts { get; set; }
        public DbSet<CommentData> Comments { get; set; }
        public DbSet<VisitData> Visits { get; set; }
        public DbSet<PaymentData> Payments { get; set; }

        public void ExecuteSqlCommand(string sql)
        {
            Database.ExecuteSqlCommand(sql);
        }

        void IDbContext.SaveChanges()
        {
            SaveChanges();
        }
    }
}
