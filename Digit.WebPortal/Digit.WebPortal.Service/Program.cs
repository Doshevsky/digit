﻿using Microsoft.AspNetCore.Hosting;
using System.IO;

namespace Digit.WebPortal.Service
{
    //public class Program
    //{
    //    public static void Main(string[] args)
    //    {
    //        BuildWebHost(args).Run();
    //    }

    //    public static IWebHost BuildWebHost(string[] args) =>
    //        WebHost.CreateDefaultBuilder(args)
    //            .UseContentRoot(Directory.GetCurrentDirectory())
    //            .UseStartup<Startup>()
    //            .Build();
    //}

    public class Program
    {
        public static void Main(string[] args)
        {
            IWebHost host = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseIISIntegration()
                .UseStartup<Startup>()
                .Build();

            host.Run();
        }
    }
}
