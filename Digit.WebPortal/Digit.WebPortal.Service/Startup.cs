﻿using Digit.WebPortal.Core;
using Digit.WebPortal.CustomerManagement;
using Digit.WebPortal.Service.Data;
using Digit.WebPortal.UserManagement;
using Digit.WebPortal.UserManagement.Model;
using JavaScriptEngineSwitcher.ChakraCore;
using JavaScriptEngineSwitcher.Extensions.MsDependencyInjection;
using JavaScriptEngineSwitcher.Msie;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using React.AspNet;
using System;
using System.Collections.Generic;

namespace Digit.WebPortal.Service
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            _services = new List<IServiceBootstrapper>
            {
                new UserManagementServiceBootstrapper(),
                new CustomerManagementServiceBootstrapper()
            };

            IConfigurationBuilder builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", false, true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            InitializeMapper();

            string connection = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<AppDbContext>(options => options.UseSqlServer(connection));

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IUserManagementDbContext, AppDbContext>();
            services.AddScoped<ICustomerManagementDbContext, AppDbContext>();

            foreach (IServiceBootstrapper service in _services)
            {
                service.Initialize(services);
            }

            services.AddJsEngineSwitcher(options => options.DefaultEngineName = ChakraCoreJsEngine.EngineName)
                .AddChakraCore()
                .AddMsie(options =>
                {
                    options.UseEcmaScript5Polyfill = true;
                    options.UseJson2Library = true;
                });

            services.AddReact();
            services.AddMvc();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            // ASPNETCORE_ENVIRONMENT = Development
            //if (env.IsDevelopment())
            //{
            //    app.UseDeveloperExceptionPage();
            //}
            //else
            //{
            //    app.UseExceptionHandler(ExceptionHandler);
            //}

            app.UseExceptionHandler(ExceptionHandler);

            app.UseReact(config => { });
            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationScheme = AuthorizationService.AuthenticationScheme,
                AutomaticAuthenticate = true,
                AutomaticChallenge = true
            });

            app.UseMvc(routes =>
            {
                // TODO: Exclude api/* routes
                routes.MapRoute("react", "{*url}", new { controller = "Home", action = "Index" });
            });

            EnsureAdminUser(app.ApplicationServices);
        }

        private void InitializeMapper()
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                foreach (IServiceBootstrapper service in _services)
                {
                    cfg.AddProfile(service.MapperProfile);
                }
            });

            //AutoMapper.Mapper.AssertConfigurationIsValid();
        }

        private void EnsureAdminUser(IServiceProvider serviceProvider)
        {
            IUserRepository userRepository = serviceProvider.GetRequiredService<IUserRepository>();
            User adminUser = userRepository.GetUserByUsername(AdminUsername);

            if (adminUser == null)
            {
                IAuthorizationService authorizationService = serviceProvider.GetRequiredService<IAuthorizationService>();
                authorizationService.Register(AdminUsername, "5vBbE3437A084a06zC5Bp244rBA27d15_1", "admin@digit.center", Role.Admin);
            }
            else if (adminUser.Role != Role.Admin)
            {
                adminUser.Role = Role.Admin;
                userRepository.UpdateUser(adminUser);
            }
        }

        private void ExceptionHandler(IApplicationBuilder appBuilder)
        {
            bool handled = false;
            appBuilder.Run(async context =>
            {
                IExceptionHandlerFeature exceptionHandlerFeature = context.Features.Get<IExceptionHandlerFeature>();
                if (exceptionHandlerFeature != null)
                {
                    if (exceptionHandlerFeature.Error is DigitAppException exception)
                    {
                        context.Response.StatusCode = (int)exception.Code;
                        await context.Response.WriteAsync(exception.Message);
                        handled = true;
                    }
                }

                if (!handled)
                {
                    context.Response.StatusCode = 500;
                    await context.Response.WriteAsync("An unexpected fault happened. Try again later.");
                }
            });
        }

        private const string AdminUsername = "admin";

        private readonly List<IServiceBootstrapper> _services;
    }
}
