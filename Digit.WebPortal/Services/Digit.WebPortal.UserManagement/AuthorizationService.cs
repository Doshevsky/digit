﻿using Digit.WebPortal.Core.Validation;
using Digit.WebPortal.UserManagement.Model;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace Digit.WebPortal.UserManagement
{
    public class AuthorizationService : IAuthorizationService
    {
        public AuthorizationService(IHttpContextAccessor httpContextAccessor, IUserRepository userRepository)
        {
            _httpContextAccessor = httpContextAccessor;
            _userRepository = userRepository;
        }

        public const string AuthenticationScheme = "CookieAuthentication";

        public User Authorize(params Role[] roles)
        {
//#if DEBUG
//            return new User
//            {
//                Id = 1,
//                Username = "DebugUser",
//                Email = "debug@email.com"
//            };
//#else
            User user = GetUserFromHttpContext();

            if (roles.Length > 0 && roles.All(x => x != user.Role))
            {
                throw new UnauthorizedException();
            }

            return user;
//#endif
        }
        public async Task Login(string username, string password)
        {
            User user = _userRepository.GetUserByUsername(username);
            if (user == null || !CheckPassword(password, user.PasswordSalt, user.PasswordHash))
            {
                throw new ValidationException("Incorrect username or password");
            }

            await SignIn(user.Id);
        }
        public async Task Logout()
        {
            //await _httpContextAccessor.HttpContext.SignOutAsync(SecurityService.AuthenticationScheme);
            await _httpContextAccessor.HttpContext.Authentication.SignOutAsync(AuthenticationScheme);
        }
        public async Task Register(string username, string password, string email, Role role)
        {
            if (_userRepository.GetUserByUsername(username) != null)
            {
                throw new ValidationException("Username already exists");
            }

            string passwordSalt = CreateSalt();
            User user = new User
            {
                Username = username,
                Email = email,
                PasswordSalt = passwordSalt,
                PasswordHash = HashPassword(password, passwordSalt),
                Role = role
            };

            _userRepository.AddUser(user);
            await SignIn(user.Id);
        }
        public async Task ChangePassword(string newPassword)
        {
            User user = Authorize();
            user.PasswordSalt = CreateSalt();
            user.PasswordHash = HashPassword(newPassword, user.PasswordSalt);

            //await _httpContextAccessor.HttpContext.SignOutAsync(SecurityService.AuthenticationScheme);
            await _httpContextAccessor.HttpContext.Authentication.SignOutAsync(AuthenticationScheme);
        }

        private async Task SignIn(int userId)
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim(UserIdClaimType, userId.ToString())
            };

            ClaimsIdentity userIdentity = new ClaimsIdentity(claims, "login");
            ClaimsPrincipal principal = new ClaimsPrincipal(userIdentity);

            //await _httpContextAccessor.HttpContext.SignInAsync(SecurityService.AuthenticationScheme, principal);
            await _httpContextAccessor.HttpContext.Authentication.SignInAsync(AuthenticationScheme, principal);
        }
        private User GetUserFromHttpContext()
        {
            ClaimsPrincipal principal = _httpContextAccessor.HttpContext.User;
            if (principal == null)
            {
                throw new UnauthorizedException();
            }

            string userIdValue = principal.Claims.FirstOrDefault(x => x.Type == UserIdClaimType)?.Value;
            if (!int.TryParse(userIdValue, out int userId))
            {
                throw new UnauthorizedException();
            }

            User user = _userRepository.GetUserById(userId);
            if (user == null)
            {
                throw new UnauthorizedException();
            }

            return user;
        }

        private string CreateSalt()
        {
            byte[] salt = new byte[128 / 8];
            using (RandomNumberGenerator randomGenerator = RandomNumberGenerator.Create())
            {
                randomGenerator.GetBytes(salt);
            }

            return Convert.ToBase64String(salt);
        }
        private string HashPassword(string password, string salt)
        {
            byte[] saltBytes = Convert.FromBase64String(salt);
            byte[] hash = KeyDerivation.Pbkdf2(password, saltBytes, KeyDerivationPrf.HMACSHA1, 10000, 256 / 8);
            return Convert.ToBase64String(hash);
        }
        private bool CheckPassword(string password, string salt, string hash)
        {
            string passwordHash = HashPassword(password, salt);
            return passwordHash == hash;
        }

        private const string UserIdClaimType = "UserId";

        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IUserRepository _userRepository;
    }
}
