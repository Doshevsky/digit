﻿using Digit.WebPortal.Core;
using Digit.WebPortal.UserManagement.Data;
using Microsoft.EntityFrameworkCore;

namespace Digit.WebPortal.UserManagement
{
    public interface IUserManagementDbContext : IDbContext
    {
        DbSet<UserData> Users { get; }
    }
}
