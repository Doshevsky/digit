﻿using Digit.WebPortal.UserManagement.Model;
using System.Collections.Generic;

namespace Digit.WebPortal.UserManagement
{
    public interface IUserRepository
    {
        IEnumerable<User> GetUsers();
        User GetUserById(int id);
        User GetUserByUsername(string username);
        void AddUser(User user);
        void UpdateUser(User user);
        void DeleteUser(int id);
    }
}
