﻿using AutoMapper;
using Digit.WebPortal.Core;
using Digit.WebPortal.UserManagement.Data;
using Microsoft.Extensions.DependencyInjection;

namespace Digit.WebPortal.UserManagement
{
    public sealed class UserManagementServiceBootstrapper : IServiceBootstrapper
    {
        public UserManagementServiceBootstrapper()
        {
            ServiceId = "UserManagement";
            MapperProfile = new UserManagementMapperProfile(ServiceId);
        }

        public string ServiceId { get; }
        public Profile MapperProfile { get; }

        public void Initialize(IServiceCollection container)
        {
            container.AddScoped<IUserRepository, SqlUserRepository>();
            container.AddScoped<IAuthorizationService, AuthorizationService>();
        }
    }
}
