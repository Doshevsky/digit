﻿namespace Digit.WebPortal.UserManagement.Model
{
    public enum Role
    {
        None = 0,
        Admin = 0x0001,
        Guest = 0x0002,
        Moderator = 0x0003,
        Teacher = 0x0004
    }
}
