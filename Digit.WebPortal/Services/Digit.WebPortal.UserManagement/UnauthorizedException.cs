﻿using Digit.WebPortal.Core;
using System.Net;

namespace Digit.WebPortal.UserManagement
{
    public class UnauthorizedException : DigitAppException
    {
        public UnauthorizedException()
            : base("Unauthorized", HttpStatusCode.Unauthorized)
        {
        }
    }
}
