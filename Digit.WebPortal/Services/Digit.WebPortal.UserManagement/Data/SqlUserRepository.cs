﻿using AutoMapper;
using Digit.WebPortal.UserManagement.Model;
using System.Collections.Generic;
using System.Linq;

namespace Digit.WebPortal.UserManagement.Data
{
    internal class SqlUserRepository : IUserRepository
    {
        public SqlUserRepository(IUserManagementDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<User> GetUsers()
        {
            IEnumerable<UserData> data = _dbContext.Users;
            return Mapper.Map<IEnumerable<User>>(data);
        }
        public User GetUserById(int id)
        {
            UserData data = GetUserData(id);
            return data == null ? null : Mapper.Map<User>(data);
        }
        public User GetUserByUsername(string username)
        {
            UserData data = _dbContext.Users.FirstOrDefault(x => x.Username == username);
            return data == null ? null : Mapper.Map<User>(data);
        }
        public void AddUser(User user)
        {
            UserData data = Mapper.Map<UserData>(user);

            _dbContext.Users.Add(data);
            _dbContext.SaveChanges();
        }
        public void UpdateUser(User user)
        {
            UserData data = GetUserData(user.Id);
            Mapper.Map(user, data);

            _dbContext.Users.Update(data);
            _dbContext.SaveChanges();
        }
        public void DeleteUser(int id)
        {
            UserData data = _dbContext.Users.FirstOrDefault(x => x.Id == id);

            _dbContext.Users.Remove(data);
            _dbContext.SaveChanges();
        }

        private UserData GetUserData(int id)
        {
            return _dbContext.Users.FirstOrDefault(x => x.Id == id);
        }

        private readonly IUserManagementDbContext _dbContext;
    }
}
