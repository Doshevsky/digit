﻿namespace Digit.WebPortal.UserManagement.Data
{
    public class UserData
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
        public int Role { get; set; }
    }
}
