﻿using AutoMapper;
using Digit.WebPortal.UserManagement.Data;
using Digit.WebPortal.UserManagement.Dto;
using Digit.WebPortal.UserManagement.Model;

namespace Digit.WebPortal.UserManagement
{
    public class UserManagementMapperProfile : Profile
    {
        public UserManagementMapperProfile(string profileName)
            : base(profileName)
        {
            // Data
            CreateMap<UserData, User>().ReverseMap();

            // Dto
            CreateMap<UserDto, User>()
                .ForMember(x => x.Role, opt => opt.MapFrom(src => (Role)src.Role));
            CreateMap<User, UserDto>()
                .ForMember(x => x.Role, opt => opt.MapFrom(src => (int)src.Role));
            CreateMap<UserUpdateDto, User>()
                .ForMember(x => x.Role, opt => opt.MapFrom(src => (Role)src.Role));
        }
    }
}
