﻿using Digit.WebPortal.Core.Validation;
using System.Linq;

namespace Digit.WebPortal.UserManagement.Dto
{
    public class UserUpdateDto
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public uint Role { get; set; }

        public void Validate()
        {
            if (!new System.ComponentModel.DataAnnotations.EmailAddressAttribute().IsValid(Email))
            {
                throw new ValidationException("Invalid Email");
            }

            if (string.IsNullOrWhiteSpace(Username) ||
                Username.Length < 3 ||
                Username.Length > 50 ||
                !Username.All(char.IsLetterOrDigit))
            {
                throw new ValidationException("Invalid Username. Username must contain only letters or digits and must contain between 3 to 50 characters");
            }
        }
    }
}
