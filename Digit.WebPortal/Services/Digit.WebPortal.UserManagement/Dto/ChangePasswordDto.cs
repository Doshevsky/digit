﻿using System.ComponentModel.DataAnnotations;

namespace Digit.WebPortal.UserManagement.Dto
{
    public class ChangePasswordDto
    {
        public string Password { get; set; }

        public void Validate()
        {
            if (string.IsNullOrWhiteSpace(Password) ||
                Password.Length < 3 ||
                Password.Length > 50)
            {
                throw new ValidationException("Invalid Password. Password must contain between 3 to 50 characters");
            }
        }
    }
}
