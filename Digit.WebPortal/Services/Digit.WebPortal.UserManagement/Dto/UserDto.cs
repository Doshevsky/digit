﻿namespace Digit.WebPortal.UserManagement.Dto
{
    public class UserDto
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public int Role { get; set; }
    }
}
