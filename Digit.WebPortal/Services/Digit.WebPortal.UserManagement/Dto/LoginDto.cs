﻿using Digit.WebPortal.Core.Validation;

namespace Digit.WebPortal.UserManagement.Dto
{
    public class LoginDto
    {
        public string Username { get; set; }
        public string Password { get; set; }

        public void Validate()
        {
            if (string.IsNullOrEmpty(Username))
            {
                throw new ValidationException("Username cannot be empty");
            }

            if (string.IsNullOrWhiteSpace(Password))
            {
                throw new ValidationException("Password cannot be empty");
            }
        }
    }
}
