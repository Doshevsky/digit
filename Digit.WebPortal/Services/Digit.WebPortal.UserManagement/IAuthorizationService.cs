﻿using Digit.WebPortal.UserManagement.Model;
using System.Threading.Tasks;

namespace Digit.WebPortal.UserManagement
{
    public interface IAuthorizationService
    {
        User Authorize(params Role[] roles);
        Task Login(string username, string password);
        Task Logout();
        Task Register(string username, string password, string email, Role role);
        Task ChangePassword(string newPassword);
    }
}