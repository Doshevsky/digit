﻿using AutoMapper;
using Digit.WebPortal.Core.Validation;
using Digit.WebPortal.UserManagement.Dto;
using Digit.WebPortal.UserManagement.Model;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Digit.WebPortal.UserManagement.Controllers
{
    [Route("auth")]
    public class AuthController : ControllerBase
    {
        public AuthController(IAuthorizationService authorizationService)
        {
            _authorizationService = authorizationService;
        }

        [HttpGet("user")]
        public IActionResult GetCurrentUser()
        {
            User user = _authorizationService.Authorize();
            UserDto dto = Mapper.Map<UserDto>(user);
            return Ok(dto);
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody]LoginDto dto)
        {
            Require.Dto(dto);
            dto.Validate();

            await _authorizationService.Login(dto.Username, dto.Password);
            return Ok();
        }

        [HttpPost("logout")]
        public async Task<IActionResult> Logout()
        {
            await _authorizationService.Logout();
            return Ok();
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody]RegisterDto dto)
        {
            Require.Dto(dto);
            dto.Validate();

            await _authorizationService.Register(dto.Username, dto.Password, dto.Email, Role.Guest);
            return Ok();
        }

        [HttpPost("password")]
        public async Task<IActionResult> ChangePassword([FromBody]ChangePasswordDto dto)
        {
            Require.Dto(dto);
            dto.Validate();

            await _authorizationService.ChangePassword(dto.Password);
            return Ok();
        }

        private readonly IAuthorizationService _authorizationService;
    }
}
