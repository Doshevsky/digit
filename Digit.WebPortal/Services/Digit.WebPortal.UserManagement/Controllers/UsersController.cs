﻿using AutoMapper;
using Digit.WebPortal.Core.Validation;
using Digit.WebPortal.UserManagement.Dto;
using Digit.WebPortal.UserManagement.Model;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Digit.WebPortal.UserManagement.Controllers
{
    [Route("api/users")]
    public class UsersController : ControllerBase
    {
        public UsersController(IAuthorizationService authorizationService, IUserRepository userRepository)
        {
            _authorizationService = authorizationService;
            _userRepository = userRepository;
        }

        [HttpGet]
        public IActionResult GetUsers()
        {
            _authorizationService.Authorize(Role.Admin);

            IEnumerable<User> users = _userRepository.GetUsers();
            IEnumerable<UserDto> dto = Mapper.Map<IEnumerable<UserDto>>(users);
            return Ok(dto);
        }

        [HttpGet("{id}")]
        public IActionResult GetUser(int id)
        {
            _authorizationService.Authorize(Role.Admin);

            User user = _userRepository.GetUserById(id);
            Require.DataObject(user);

            UserDto dto = Mapper.Map<UserDto>(user);
            return Ok(dto);
        }

        [HttpPut("{Id}")]
        public IActionResult UpdateUser(int id, [FromBody]UserUpdateDto dto)
        {
            _authorizationService.Authorize(Role.Admin);

            Require.Dto(dto);
            dto.Validate();

            User user = _userRepository.GetUserById(id);
            Require.DataObject(user);

            if (user.Username != dto.Username && _userRepository.GetUserByUsername(dto.Username) != null)
            {
                throw new ValidationException("Username already exists");
            }

            Mapper.Map(dto, user);

            _userRepository.UpdateUser(user);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteUser(int id)
        {
            _authorizationService.Authorize(Role.Admin);

            User user = _userRepository.GetUserById(id);
            Require.DataObject(user);

            _userRepository.DeleteUser(id);
            return NoContent();
        }

        private readonly IAuthorizationService _authorizationService;
        private readonly IUserRepository _userRepository;
    }
}
