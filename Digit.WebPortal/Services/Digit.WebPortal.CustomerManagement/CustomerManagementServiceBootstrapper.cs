﻿using AutoMapper;
using Digit.WebPortal.Core;
using Digit.WebPortal.CustomerManagement.Data;
using Microsoft.Extensions.DependencyInjection;

namespace Digit.WebPortal.CustomerManagement
{
    public sealed class CustomerManagementServiceBootstrapper : IServiceBootstrapper
    {
        public CustomerManagementServiceBootstrapper()
        {
            ServiceId = "CustomerManagement";
            MapperProfile = new CustomerManagementMapperProfile(ServiceId);
        }

        public string ServiceId { get; }
        public Profile MapperProfile { get; }

        public void Initialize(IServiceCollection container)
        {
            container.AddScoped<ICustomerRepository, SqlCustomerRepository>();
            container.AddScoped<ICustomerManager, CustomerManager>();
        }
    }
}
