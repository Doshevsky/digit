﻿using System.Collections.Generic;

namespace Digit.WebPortal.CustomerManagement.Dto
{
    public class RepresentativeDto
    {
        public RepresentativeDto()
        {
            Contacts = new List<ContactDto>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Comment { get; set; }
        public List<ContactDto> Contacts { get; set; }
    }
}
