﻿using System;

namespace Digit.WebPortal.CustomerManagement.Dto
{
    public class ActionDto
    {
        public string Message { get; set; }
        public DateTime DueDate { get; set; }
        public bool Overdue { get; set; }
    }
}
