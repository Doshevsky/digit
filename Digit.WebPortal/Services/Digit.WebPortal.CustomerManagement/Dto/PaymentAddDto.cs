﻿using Digit.WebPortal.Core.Validation;
using System;

namespace Digit.WebPortal.CustomerManagement.Dto
{
    public class PaymentAddDto
    {
        public DateTime Created { get; set; }
        public DateTime PeriodStart { get; set; }
        public DateTime PeriodEnd { get; set; }
        public int Amount { get; set; }

        public void Validate()
        {
            if (PeriodEnd <= PeriodStart)
            {
                throw new ValidationException("Period end date must be greater than Period start date");
            }

            if (Amount < 1)
            {
                throw new ValidationException("Amount must be greater than 0");
            }
        }
    }
}
