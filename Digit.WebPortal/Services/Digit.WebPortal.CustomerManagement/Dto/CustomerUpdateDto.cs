﻿using Digit.WebPortal.Core.Validation;
using Digit.WebPortal.CustomerManagement.Model;
using System;
using System.Collections.Generic;

namespace Digit.WebPortal.CustomerManagement.Dto
{
    public class CustomerUpdateDto
    {
        public CustomerUpdateDto()
        {
            Contacts = new List<ContactUpdateDto>();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Information { get; set; }
        public int Status { get; set; }
        public int Channel { get; set; }
        public List<ContactUpdateDto> Contacts { get; set; }

        public void Validate()
        {
            StringValidation.CheckEmptyOr50Characters(FirstName, $"{nameof(Customer)} {nameof(FirstName)}");
            StringValidation.CheckEmptyOr50Characters(LastName, $"{nameof(Customer)} {nameof(LastName)}");
            StringValidation.CheckEmptyOr250Characters(Information, $"{nameof(Customer)} {nameof(Information)}");
            EnumValidation.CheckRange(typeof(Status), Status, $"{nameof(Customer)} {nameof(Status)}");
            EnumValidation.CheckRange(typeof(Channel), Channel, $"{nameof(Customer)} {nameof(Channel)}");

            foreach (ContactUpdateDto contact in Contacts)
            {
                contact.Validate();
            }
        }
    }
}
