﻿using Digit.WebPortal.Core.Validation;
using Digit.WebPortal.CustomerManagement.Model;
using System.Collections.Generic;

namespace Digit.WebPortal.CustomerManagement.Dto
{
    public class RepresentativeUpdateDto
    {
        public RepresentativeUpdateDto()
        {
            Contacts = new List<ContactUpdateDto>();
        }

        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Comment { get; set; }
        public List<ContactUpdateDto> Contacts { get; set; }

        public void Validate()
        {
            StringValidation.CheckEmptyOr50Characters(FirstName, $"{nameof(Representative)} {nameof(FirstName)}");
            StringValidation.CheckEmptyOr50Characters(MiddleName, $"{nameof(Representative)} {nameof(MiddleName)}");
            StringValidation.CheckEmptyOr50Characters(LastName, $"{nameof(Representative)} {nameof(LastName)}");
            StringValidation.CheckEmptyOr250Characters(Comment, $"{nameof(Representative)} {nameof(Comment)}");
        }
    }
}
