﻿using Digit.WebPortal.Core.Validation;
using System;

namespace Digit.WebPortal.CustomerManagement.Dto
{
    public class VisitAddDto
    {
        public DateTime Start { get; set; }
        public DateTime End { get; set; }

        public void Validate()
        {
            if (End < Start)
            {
                throw new ValidationException("End date must be greater than Start date");
            }
        }
    }
}
