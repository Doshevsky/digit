﻿using System;

namespace Digit.WebPortal.CustomerManagement.Dto
{
    public class VisitDto
    {
        public int Id { get; set; }
        public DateTime Start { get; set; }
        public DateTime? End { get; set; }
        public TimeSpan? Duration { get; set; }
    }
}
