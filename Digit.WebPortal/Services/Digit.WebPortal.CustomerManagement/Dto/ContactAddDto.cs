﻿using Digit.WebPortal.Core.Validation;
using Digit.WebPortal.CustomerManagement.Model;

namespace Digit.WebPortal.CustomerManagement.Dto
{
    public class ContactAddDto
    {
        public int Type { get; set; }
        public string Value { get; set; }

        public void Validate()
        {
            EnumValidation.CheckRange(typeof(ContactType), Type, $"{nameof(Contact)} {nameof(Type)}");
            StringValidation.CheckNonEmptyAnd50Characters(Value, $"{nameof(Contact)} {nameof(Value)}");
        }
    }
}
