﻿using Digit.WebPortal.Core.Validation;
using System;

namespace Digit.WebPortal.CustomerManagement.Dto
{
    public class ActionUpdateDto
    {
        public string Message { get; set; }
        public DateTime DueDate { get; set; }

        public void Validate()
        {
            DateTimeValidation.CheckNonDefault(DueDate, $"{nameof(Action)} {nameof(DueDate)}");
            StringValidation.CheckNonEmptyAnd250Characters(Message, $"{nameof(Action)} {nameof(Message)}");
        }
    }
}
