﻿namespace Digit.WebPortal.CustomerManagement.Dto
{
    public class ContactDto
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public string Value { get; set; }
    }
}
