﻿using Digit.WebPortal.Core.Validation;
using Digit.WebPortal.CustomerManagement.Model;

namespace Digit.WebPortal.CustomerManagement.Dto
{
    public class CommentUpdateDto
    {
        public string Message { get; set; }

        public void Validate()
        {
            StringValidation.CheckNonEmptyAnd350Characters(Message, $"{nameof(Comment)} {nameof(Message)}");
        }
    }
}
