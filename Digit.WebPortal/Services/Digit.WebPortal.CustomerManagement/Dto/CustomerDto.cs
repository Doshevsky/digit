﻿using System;
using System.Collections.Generic;

namespace Digit.WebPortal.CustomerManagement.Dto
{
    public class CustomerDto
    {
        public CustomerDto()
        {
            Contacts = new List<ContactDto>();
            Representatives = new List<RepresentativeDto>();
            Comments = new List<CommentDto>();
            Visits = new List<VisitDto>();
            Payments = new List<PaymentDto>();
        }

        public int Id { get; set; }
        public DateTime Created { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int? Age { get; set; }
        public string Information { get; set; }
        public ActionDto Action { get; set; }
        public int Status { get; set; }
        public int Channel { get; set; }
        public List<ContactDto> Contacts { get; set; }
        public List<RepresentativeDto> Representatives { get; set; }
        public List<CommentDto> Comments { get; set; }
        public List<VisitDto> Visits { get; set; }
        public List<PaymentDto> Payments { get; set; }

        public bool PaymentSoonExpire { get; set; }
        public bool PaymentExpired { get; set; }
        public bool NotAttend { get; set; }
    }
}
