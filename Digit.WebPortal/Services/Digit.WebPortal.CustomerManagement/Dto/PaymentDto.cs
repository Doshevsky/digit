﻿using System;

namespace Digit.WebPortal.CustomerManagement.Dto
{
    public class PaymentDto
    {
        public int Id { get; set; }
        public DateTime Created { get; set; }
        public DateTime PeriodStart { get; set; }
        public DateTime PeriodEnd { get; set; }
        public int Days { get; set; }
        public int Amount { get; set; }
    }
}
