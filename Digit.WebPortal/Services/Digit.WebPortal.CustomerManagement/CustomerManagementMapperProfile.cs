﻿using AutoMapper;
using Digit.WebPortal.CustomerManagement.Data;
using Digit.WebPortal.CustomerManagement.Dto;
using Digit.WebPortal.CustomerManagement.Model;
using System.Linq;

namespace Digit.WebPortal.CustomerManagement
{
    public class CustomerManagementMapperProfile : Profile
    {
        public CustomerManagementMapperProfile(string profileName)
            : base(profileName)
        {
            // Data
            CreateMap<ActionData, Action>().ReverseMap();
            CreateMap<ContactData, Contact>().ReverseMap();
            CreateMap<RepresentativeData, Representative>().ReverseMap();
            CreateMap<CommentData, Comment>().ReverseMap();
            CreateMap<VisitData, Visit>().ReverseMap();
            CreateMap<PaymentData, Payment>().ReverseMap();
            CreateMap<CustomerData, Customer>()
                .ForMember(x => x.Action, opt => opt.MapFrom(src => src.Actions.FirstOrDefault()));
            CreateMap<Customer, CustomerData>()
                .ForMember(x => x.Actions, opt => opt.Ignore())
                .ForMember(x => x.Representatives, opt => opt.Ignore())
                .ForMember(x => x.Comments, opt => opt.Ignore())
                .ForMember(x => x.Visits, opt => opt.Ignore())
                .ForMember(x => x.Payments, opt => opt.Ignore());

            // Dto
            CreateMap<Action, ActionDto>();
            CreateMap<ActionUpdateDto, Action>();

            CreateMap<Contact, ContactDto>();
            CreateMap<ContactAddDto, Contact>();
            CreateMap<ContactUpdateDto, Contact>().ReverseMap();

            CreateMap<Representative, RepresentativeDto>();
            CreateMap<RepresentativeAddDto, Representative>();
            CreateMap<RepresentativeUpdateDto, Representative>();

            CreateMap<Comment, CommentDto>()
                .ForMember(x => x.Author, opt => opt.MapFrom(src => src.Author.Username));
            CreateMap<CommentAddDto, Comment>();
            CreateMap<CommentUpdateDto, Comment>();

            CreateMap<Visit, VisitDto>();
            CreateMap<VisitAddDto, Visit>();

            CreateMap<Payment, PaymentDto>();
            CreateMap<PaymentAddDto, Payment>();

            CreateMap<Customer, CustomerDto>();
            CreateMap<CustomerAddDto, Customer>();
            CreateMap<CustomerUpdateDto, Customer>().ReverseMap();
        }
    }
}
