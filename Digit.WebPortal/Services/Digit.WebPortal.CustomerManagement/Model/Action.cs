﻿using System;

namespace Digit.WebPortal.CustomerManagement.Model
{
    public class Action
    {
        public string Message { get; set; }
        public DateTime DueDate{ get; set; }

        public bool Overdue { get; set; }
    }
}
