﻿namespace Digit.WebPortal.CustomerManagement.Model
{
    public enum ContactType
    {
        Other,
        Address,
        Phone,
        Email,
        School
    }
}
