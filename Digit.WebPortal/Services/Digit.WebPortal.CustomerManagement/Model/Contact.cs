﻿namespace Digit.WebPortal.CustomerManagement.Model
{
    public class Contact
    {
        public int Id { get; set; }
        public ContactType Type { get; set; }
        public string Value { get; set; }
    }
}
