﻿namespace Digit.WebPortal.CustomerManagement.Model
{
    public enum Status
    {
        Newbie = 0x0001,
        Waiting = 0x0002,
        Active = 0x0003,
        Rejection = 0x0004
    }
}
