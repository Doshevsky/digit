﻿using System;
using System.Collections.Generic;

namespace Digit.WebPortal.CustomerManagement.Model
{
    public class Customer
    {
        public Customer()
        {
            Contacts = new List<Contact>();
            Representatives = new List<Representative>();
            Comments = new List<Comment>();
            Visits = new List<Visit>();
            Payments = new List<Payment>();
        }

        public int Id { get; set; }
        public DateTime Created { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int? Age { get; set; }
        public string Information { get; set; }
        public Action Action { get; set; }
        public Status Status { get; set; }
        public Channel Channel { get; set; }
        public List<Contact> Contacts { get; set; }
        public List<Representative> Representatives { get; set; }
        public List<Comment> Comments { get; set; }
        public List<Visit> Visits { get; set; }
        public List<Payment> Payments { get; set; }

        public bool PaymentSoonExpire { get; set; }
        public bool PaymentExpired { get; set; }
        public bool NotAttend { get; set; }
    }
}
