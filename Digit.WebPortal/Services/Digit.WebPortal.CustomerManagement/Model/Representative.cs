﻿using System.Collections.Generic;

namespace Digit.WebPortal.CustomerManagement.Model
{
    public class Representative
    {
        public Representative()
        {
            Contacts = new List<Contact>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Comment { get; set; }
        public List<Contact> Contacts { get; set; }
    }
}
