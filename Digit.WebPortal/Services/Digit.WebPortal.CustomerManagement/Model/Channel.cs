﻿namespace Digit.WebPortal.CustomerManagement.Model
{
    public enum Channel
    {
        None = 0,
        Recommendation = 0x0001,
        Yandex = 0x0002,
        SocialVk = 0x0003
    }
}
