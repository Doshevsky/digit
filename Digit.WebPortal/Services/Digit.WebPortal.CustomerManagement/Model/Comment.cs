﻿using Digit.WebPortal.UserManagement.Model;
using System;

namespace Digit.WebPortal.CustomerManagement.Model
{
    public class Comment
    {
        public int Id { get; set; }
        public DateTime Created { get; set; }
        public User Author { get; set; }
        public string Message { get; set; }
    }
}
