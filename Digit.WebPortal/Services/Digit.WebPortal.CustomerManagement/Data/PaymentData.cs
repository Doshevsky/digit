﻿using System;

namespace Digit.WebPortal.CustomerManagement.Data
{
    public class PaymentData
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public DateTime Created { get; set; }
        public DateTime PeriodStart { get; set; }
        public DateTime PeriodEnd { get; set; }
        public int Amount { get; set; }
    }
}
