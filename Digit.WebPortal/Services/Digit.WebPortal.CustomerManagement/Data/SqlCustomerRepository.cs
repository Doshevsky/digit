﻿using AutoMapper;
using Digit.WebPortal.CustomerManagement.Model;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Digit.WebPortal.CustomerManagement.Data
{
    internal class SqlCustomerRepository : ICustomerRepository
    {
        public SqlCustomerRepository(ICustomerManagementDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<Customer> GetCustomers()
        {
            IEnumerable<CustomerData> data = _dbContext
                .Customers
                .Include(x => x.Actions)
                .Include(x => x.Visits)
                .Include(x => x.Payments)
                .Include(x => x.Contacts);

            return Mapper.Map<IEnumerable<Customer>>(data);
        }
        public Customer GetCustomerById(int id)
        {
            CustomerData data = _dbContext
                .Customers
                .Include(x => x.Actions)
                .Include(x => x.Contacts)
                .Include(x => x.Representatives)
                    .ThenInclude(x => x.Contacts)
                .Include(x => x.Comments)
                    .ThenInclude(x => x.Author)
                .Include(x => x.Visits)
                .Include(x => x.Payments)
                .FirstOrDefault(x => x.Id == id);

            return data == null ? null : Mapper.Map<Customer>(data);
        }
        public void AddCustomer(Customer customer)
        {
            CustomerData data = Mapper.Map<CustomerData>(customer);

            _dbContext.Customers.Add(data);
            _dbContext.SaveChanges();

            customer.Id = data.Id;
        }
        public void UpdateCustomer(Customer customer)
        {
            CustomerData data = _dbContext.Customers.FirstOrDefault(x => x.Id == customer.Id);
            Mapper.Map(customer, data);

            _dbContext.Customers.Update(data);
            _dbContext.SaveChanges();

            DeleteNullContacts();
        }
        public void DeleteCustomer(int id)
        {
            CustomerData data = _dbContext.Customers.FirstOrDefault(x => x.Id == id);

            _dbContext.Customers.Remove(data);
            _dbContext.SaveChanges();

            DeleteNullContacts();
        }

        public void SetAction(int customerId, Action action)
        {
            ActionData data = _dbContext.Actions.FirstOrDefault(x => x.CustomerId == customerId);
            if (data != null)
            {
                Mapper.Map(action, data);
                _dbContext.Actions.Update(data);
            }
            else
            {
                data = Mapper.Map<ActionData>(action);
                data.CustomerId = customerId;
                _dbContext.Actions.Add(data);
            }

            _dbContext.SaveChanges();
        }
        public void DeleteAction(int customerId)
        {
            IEnumerable<ActionData> data = _dbContext.Actions.Where(x => x.CustomerId == customerId).ToList();
            _dbContext.Actions.RemoveRange(data);
            _dbContext.SaveChanges();
        }

        public Representative GetRepresentativeById(int id)
        {
            RepresentativeData data = _dbContext
                .Representatives
                .Include(x => x.Contacts)
                .FirstOrDefault(x => x.Id == id);

            return data == null ? null : Mapper.Map<Representative>(data);
        }
        public void AddRepresentative(int customerId, Representative representative)
        {
            RepresentativeData data = Mapper.Map<RepresentativeData>(representative);
            data.CustomerId = customerId;

            _dbContext.Representatives.Add(data);
            _dbContext.SaveChanges();

            representative.Id = data.Id;
        }
        public void UpdateRepresentative(Representative representative)
        {
            RepresentativeData data = _dbContext.Representatives.FirstOrDefault(x => x.Id == representative.Id);
            Mapper.Map(representative, data);

            _dbContext.Representatives.Update(data);
            _dbContext.SaveChanges();
        }
        public void DeleteRepresentative(int id)
        {
            RepresentativeData data = _dbContext.Representatives.FirstOrDefault(x => x.Id == id);

            _dbContext.Representatives.Remove(data);
            _dbContext.SaveChanges();
        }

        public Visit GetVisitById(int id)
        {
            VisitData data = _dbContext.Visits.FirstOrDefault(x => x.Id == id);
            return data == null ? null : Mapper.Map<Visit>(data);
        }
        public void AddVisit(int customerId, Visit visit)
        {
            VisitData data = Mapper.Map<VisitData>(visit);
            data.CustomerId = customerId;

            _dbContext.Visits.Add(data);
            _dbContext.SaveChanges();

            visit.Id = data.Id;
        }
        public void DeleteVisit(int id)
        {
            VisitData data = _dbContext.Visits.FirstOrDefault(x => x.Id == id);

            _dbContext.Visits.Remove(data);
            _dbContext.SaveChanges();
        }

        public Comment GetCommentById(int id)
        {
            CommentData data = _dbContext.Comments
                .Include(x => x.Author)
                .FirstOrDefault(x => x.Id == id);

            return data == null ? null : Mapper.Map<Comment>(data);
        }
        public void AddComment(int customerId, int authorId, Comment comment)
        {
            CommentData data = Mapper.Map<CommentData>(comment);
            data.CustomerId = customerId;
            data.AuthorId = authorId;

            _dbContext.Comments.Add(data);
            _dbContext.SaveChanges();

            comment.Id = data.Id;
        }
        public void UpdateComment(Comment comment)
        {
            CommentData data = _dbContext.Comments.FirstOrDefault(x => x.Id == comment.Id);
            Mapper.Map(comment, data);

            _dbContext.Comments.Update(data);
            _dbContext.SaveChanges();
        }
        public void DeleteComment(int id)
        {
            CommentData data = _dbContext.Comments.FirstOrDefault(x => x.Id == id);

            _dbContext.Comments.Remove(data);
            _dbContext.SaveChanges();
        }

        public Payment GetPaymentById(int id)
        {
            PaymentData data = _dbContext.Payments.FirstOrDefault(x => x.Id == id);
            return data == null ? null : Mapper.Map<Payment>(data);
        }
        public void AddPayment(int customerId, Payment payment)
        {
            PaymentData data = Mapper.Map<PaymentData>(payment);
            data.CustomerId = customerId;

            _dbContext.Payments.Add(data);
            _dbContext.SaveChanges();

            payment.Id = data.Id;
        }
        public void DeletePayment(int id)
        {
            PaymentData data = _dbContext.Payments.FirstOrDefault(x => x.Id == id);

            _dbContext.Payments.Remove(data);
            _dbContext.SaveChanges();
        }

        public IEnumerable<Customer> Search(string searchQuery)
        {
            IEnumerable<CustomerData> data = _dbContext.Customers.Where(x =>
                x.FirstName.Contains(searchQuery) ||
                x.LastName.Contains(searchQuery) ||
                x.Contacts.Any(m =>
                    m.Value.Contains(searchQuery)) ||
                x.Representatives.Any(m =>
                    m.FirstName.Contains(searchQuery) ||
                    m.MiddleName.Contains(searchQuery) ||
                    m.LastName.Contains(searchQuery) ||
                    m.Contacts.Any(n =>
                        n.Value.Contains(searchQuery))));

            return Mapper.Map<IEnumerable<Customer>>(data);
        }

        private void DeleteNullContacts()
        {
            _dbContext.ExecuteSqlCommand("DELETE FROM Contacts WHERE CustomerId IS NULL AND RepresentativeId IS NULL;");
        }

        private readonly ICustomerManagementDbContext _dbContext;
    }
}
