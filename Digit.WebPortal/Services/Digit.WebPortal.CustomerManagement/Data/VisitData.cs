﻿using System;

namespace Digit.WebPortal.CustomerManagement.Data
{
    public class VisitData
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public DateTime Start { get; set; }
        public DateTime? End { get; set; }
    }
}
