﻿using System;

namespace Digit.WebPortal.CustomerManagement.Data
{
    public class ActionData
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string Message { get; set; }
        public DateTime DueDate { get; set; }
    }
}
