﻿namespace Digit.WebPortal.CustomerManagement.Data
{
    public class ContactData
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public string Value { get; set; }
    }
}
