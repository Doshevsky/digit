﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Digit.WebPortal.CustomerManagement.Data
{
    public class CustomerData
    {
        public CustomerData()
        {
            Contacts = new List<ContactData>();
            Representatives = new List<RepresentativeData>();
            Comments = new List<CommentData>();
            Visits = new List<VisitData>();
            Payments = new List<PaymentData>();
        }

        public int Id { get; set; }
        public DateTime Created { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Information { get; set; }
        public int Status { get; set; }
        public int Channel { get; set; }

        [ForeignKey("CustomerId")]
        public List<ActionData> Actions { get; set; }

        [ForeignKey("CustomerId")]
        public List<ContactData> Contacts { get; set; }

        [ForeignKey("CustomerId")]
        public List<RepresentativeData> Representatives { get; set; }

        [ForeignKey("CustomerId")]
        public List<CommentData> Comments { get; set; }

        [ForeignKey("CustomerId")]
        public List<VisitData> Visits { get; set; }

        [ForeignKey("CustomerId")]
        public List<PaymentData> Payments { get; set; }
    }
}
