﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Digit.WebPortal.CustomerManagement.Data
{
    public class RepresentativeData
    {
        public RepresentativeData()
        {
            Contacts = new List<ContactData>();
        }

        public int Id { get; set; }
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Comment { get; set; }

        [ForeignKey("RepresentativeId")]
        public List<ContactData> Contacts { get; set; }
    }
}
