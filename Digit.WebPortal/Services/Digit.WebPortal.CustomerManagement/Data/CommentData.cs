﻿using Digit.WebPortal.UserManagement.Data;
using System;

namespace Digit.WebPortal.CustomerManagement.Data
{
    public class CommentData
    {
        public int Id { get; set; }
        public int CustomerId { get; set; }
        public DateTime Created { get; set; }
        public UserData Author { get; set; }
        public int AuthorId { get; set; }
        public string Message { get; set; }
    }
}
