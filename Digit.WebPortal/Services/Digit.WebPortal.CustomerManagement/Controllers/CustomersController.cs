﻿using System;
using AutoMapper;
using Digit.WebPortal.Core.Validation;
using Digit.WebPortal.CustomerManagement.Dto;
using Digit.WebPortal.CustomerManagement.Model;
using Digit.WebPortal.UserManagement;
using Digit.WebPortal.UserManagement.Model;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Action = Digit.WebPortal.CustomerManagement.Model.Action;

namespace Digit.WebPortal.CustomerManagement.Controllers
{
    [Route("api/customers")]
    public class CustomersController : Controller
    {
        public CustomersController(IAuthorizationService authorizationService, ICustomerManager customerManager)
        {
            _authorizationService = authorizationService;
            _customerManager = customerManager;
        }

        [HttpGet]
        public IActionResult GetCustomers(CustomersResourceParameters parameters)
        {
            _authorizationService.Authorize(Role.Admin, Role.Moderator, Role.Teacher);

            IEnumerable<Customer> customers;
            if (string.IsNullOrWhiteSpace(parameters.SearchQuery))
            {
                customers = _customerManager.GetCustomers();
            }
            else
            {
                customers = _customerManager.Search(parameters.SearchQuery);
            }

            if (parameters.Status.HasValue)
            {
                customers = customers.Where(x => (int)x.Status == parameters.Status.Value);
            }

            switch (parameters.Sort)
            {
                case CustomersSortType.Name:
                    customers = customers
                        .OrderBy(x => x.LastName, StringNullComparer.Instance)
                        .ThenBy(x => x.FirstName, StringNullComparer.Instance);
                    break;

                case CustomersSortType.ActionDueDate:
                    customers = customers.OrderBy(x => x.Action?.DueDate ?? DateTime.MaxValue);
                    break;
            }

            IEnumerable<CustomerDto> dto = Mapper.Map<IEnumerable<CustomerDto>>(customers);
            return Ok(dto);
        }

        [HttpGet("{id}", Name = "GetCustomer")]
        public IActionResult GetCustomer(int id)
        {
            _authorizationService.Authorize(Role.Admin, Role.Moderator, Role.Teacher);

            Customer customer = _customerManager.GetCustomerById(id);
            CustomerDto dto = Mapper.Map<CustomerDto>(customer);
            return Ok(dto);
        }

        [HttpPost]
        public IActionResult AddCustomer([FromBody]CustomerAddDto dto)
        {
            _authorizationService.Authorize(Role.Admin, Role.Moderator);

            Require.Dto(dto);
            dto.Validate();

            Customer customer = Mapper.Map<Customer>(dto);
            _customerManager.AddCustomer(customer);

            CustomerDto createDto = Mapper.Map<CustomerDto>(customer);
            return CreatedAtRoute("GetCustomer", new { id = createDto.Id }, createDto);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateCustomer(int id, [FromBody] CustomerUpdateDto dto)
        {
            _authorizationService.Authorize(Role.Admin, Role.Moderator);

            Customer customer = _customerManager.GetCustomerById(id);
            Require.DataObject(customer);

            Require.Dto(dto);
            dto.Validate();

            Mapper.Map(dto, customer);

            _customerManager.UpdateCustomer(customer);
            return NoContent();
        }

        [HttpPatch("{id}")]
        public IActionResult PartiallyUpdateCustomer(int id, [FromBody] JsonPatchDocument<CustomerUpdateDto> patchDoc)
        {
            _authorizationService.Authorize(Role.Admin, Role.Moderator);

            Customer customer = _customerManager.GetCustomerById(id);
            Require.DataObject(customer);

            Require.Dto(patchDoc);

            CustomerUpdateDto dto = Mapper.Map<CustomerUpdateDto>(customer);
            PatchHelper.Apply(patchDoc, dto);
            dto.Validate();

            Mapper.Map(dto, customer);

            _customerManager.UpdateCustomer(customer);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteCustomer(int id)
        {
            _authorizationService.Authorize(Role.Admin);

            Customer customer = _customerManager.GetCustomerById(id);
            Require.DataObject(customer);

            _customerManager.DeleteCustomer(id);
            return NoContent();
        }

        // ========================

        [HttpPost("{id}/action")]
        public IActionResult SetAction(int id, [FromBody]ActionUpdateDto dto)
        {
            _authorizationService.Authorize(Role.Admin, Role.Moderator);

            Customer customer = _customerManager.GetCustomerById(id);
            Require.DataObject(customer);

            Require.Dto(dto);
            dto.Validate();

            Action action = Mapper.Map<Action>(dto);
            _customerManager.SetAction(customer.Id, action);
            return NoContent();
        }

        [HttpDelete("{id}/action")]
        public IActionResult DeleteAction(int id)
        {
            _authorizationService.Authorize(Role.Admin, Role.Moderator);

            Customer customer = _customerManager.GetCustomerById(id);
            Require.DataObject(customer);

            _customerManager.DeleteAction(customer.Id);
            return NoContent();
        }

        // ========================

        [HttpGet("representatives/{id}")]
        public IActionResult GetRepresentative(int id)
        {
            _authorizationService.Authorize(Role.Admin, Role.Moderator, Role.Teacher);

            Representative representative = _customerManager.GetRepresentativeById(id);
            Require.DataObject(representative);

            RepresentativeDto dto = Mapper.Map<RepresentativeDto>(representative);
            return Ok(dto);
        }

        [HttpPost("{id}/representatives")]
        public IActionResult AddRepresentative(int id, [FromBody] RepresentativeAddDto dto)
        {
            _authorizationService.Authorize(Role.Admin, Role.Moderator);

            Require.Dto(dto);
            dto.Validate();

            Customer customer = _customerManager.GetCustomerById(id);
            Require.DataObject(customer);

            Representative representative = Mapper.Map<Representative>(dto);
            _customerManager.AddRepresentative(customer.Id, representative);

            return NoContent();
        }

        [HttpPut("representatives/{id}")]
        public IActionResult UpdateRepresentative(int id, [FromBody] RepresentativeUpdateDto dto)
        {
            _authorizationService.Authorize(Role.Admin, Role.Moderator);

            Representative representative = _customerManager.GetRepresentativeById(id);
            Require.DataObject(representative);

            Require.Dto(dto);
            dto.Validate();

            Mapper.Map(dto, representative);

            _customerManager.UpdateRepresentative(representative);
            return NoContent();
        }

        [HttpDelete("representatives/{id}")]
        public IActionResult DeleteRepresentative(int id)
        {
            _authorizationService.Authorize(Role.Admin, Role.Moderator);

            _customerManager.DeleteRepresentative(id);
            return NoContent();
        }

        // ========================

        [HttpGet("comments/{id}")]
        public IActionResult GetComment(int id)
        {
            _authorizationService.Authorize(Role.Admin, Role.Moderator, Role.Teacher);

            Comment comment = _customerManager.GetCommentById(id);
            Require.DataObject(comment);

            CommentDto dto = Mapper.Map<CommentDto>(comment);
            return Ok(dto);
        }

        [HttpPost("{id}/comments")]
        public IActionResult AddComment(int id, [FromBody] CommentAddDto dto)
        {
            User user = _authorizationService.Authorize(Role.Admin, Role.Moderator, Role.Teacher);

            Require.Dto(dto);
            dto.Validate();

            Customer customer = _customerManager.GetCustomerById(id);
            Require.DataObject(customer);

            Comment comment = Mapper.Map<Comment>(dto);
            _customerManager.AddComment(customer.Id, user.Id, comment);

            return NoContent();
        }

        [HttpPut("comments/{id}")]
        public IActionResult UpdateComment(int id, [FromBody] CommentUpdateDto dto)
        {
            User user = _authorizationService.Authorize(Role.Admin, Role.Moderator, Role.Teacher);

            Comment comment = _customerManager.GetCommentById(id);
            Require.DataObject(comment);

            if (user.Role == Role.Teacher && comment.Author.Id != user.Id)
            {
                throw new UnauthorizedException();
            }

            Require.Dto(dto);
            dto.Validate();

            Mapper.Map(dto, comment);

            _customerManager.UpdateComment(comment);
            return NoContent();
        }

        [HttpDelete("comments/{id}")]
        public IActionResult DeleteComment(int id)
        {
            User user = _authorizationService.Authorize(Role.Admin, Role.Moderator, Role.Teacher);

            Comment comment = _customerManager.GetCommentById(id);
            Require.DataObject(comment);

            if (user.Role == Role.Teacher && comment.Author.Id != user.Id)
            {
                throw new UnauthorizedException();
            }

            _customerManager.DeleteComment(id);
            return NoContent();
        }

        // ========================

        [HttpPost("{id}/visits")]
        public IActionResult AddVisit(int id, [FromBody] VisitAddDto dto)
        {
            _authorizationService.Authorize(Role.Admin, Role.Moderator, Role.Teacher);

            Require.Dto(dto);
            dto.Validate();

            Customer customer = _customerManager.GetCustomerById(id);
            Require.DataObject(customer);

            Visit visit = Mapper.Map<Visit>(dto);
            _customerManager.AddVisit(customer.Id, visit);

            return NoContent();
        }

        [HttpDelete("visits/{id}")]
        public IActionResult DeleteVisit(int id)
        {
            _authorizationService.Authorize(Role.Admin, Role.Moderator);

            _customerManager.DeleteVisit(id);
            return NoContent();
        }

        // ========================

        [HttpPost("{id}/payments")]
        public IActionResult AddPayment(int id, [FromBody] PaymentAddDto dto)
        {
            _authorizationService.Authorize(Role.Admin, Role.Moderator);

            Require.Dto(dto);
            dto.Validate();

            Customer customer = _customerManager.GetCustomerById(id);
            Require.DataObject(customer);

            Payment payment = Mapper.Map<Payment>(dto);
            _customerManager.AddPayment(customer.Id, payment);

            return NoContent();
        }

        [HttpDelete("payments/{id}")]
        public IActionResult DeletePayment(int id)
        {
            _authorizationService.Authorize(Role.Admin, Role.Moderator);

            _customerManager.DeletePayment(id);
            return NoContent();
        }

        private readonly IAuthorizationService _authorizationService;
        private readonly ICustomerManager _customerManager;
    }
}
