﻿using System.Collections.Generic;

namespace Digit.WebPortal.CustomerManagement.Controllers
{
    internal class StringNullComparer : IComparer<string>
    {
        static StringNullComparer()
        {
            Instance = new StringNullComparer();
        }

        public static StringNullComparer Instance { get; }

        public int Compare(string x, string y)
        {
            if (string.IsNullOrEmpty(x))
            {
                return 1;
            }

            if (string.IsNullOrEmpty(y))
            {
                return -1;
            }

            return x.CompareTo(y);
        }
    }
}
