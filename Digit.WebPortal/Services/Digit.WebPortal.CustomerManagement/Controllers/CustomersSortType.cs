﻿namespace Digit.WebPortal.CustomerManagement.Controllers
{
    public enum CustomersSortType
    {
        Default,
        Name,
        ActionDueDate
    }
}
