﻿namespace Digit.WebPortal.CustomerManagement.Controllers
{
    public class CustomersResourceParameters
    {
        public int? Status { get; set; }
        public CustomersSortType Sort { get; set; }
        public string SearchQuery { get; set; }
    }
}
