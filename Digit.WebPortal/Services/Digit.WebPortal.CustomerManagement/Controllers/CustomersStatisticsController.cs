﻿using AutoMapper;
using Digit.WebPortal.CustomerManagement.Dto;
using Digit.WebPortal.CustomerManagement.Model;
using Digit.WebPortal.UserManagement;
using Digit.WebPortal.UserManagement.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Digit.WebPortal.CustomerManagement.Controllers
{
    [Route("api/customers/statistics")]
    public class CustomersStatisticsController : Controller
    {
        public CustomersStatisticsController(IAuthorizationService authorizationService, ICustomerManager customerManager)
        {
            _authorizationService = authorizationService;
            _customerManager = customerManager;
        }

        [HttpGet("visitsbyday")]
        public IActionResult VisitsByDay(DateTime date)
        {
            _authorizationService.Authorize(Role.Admin, Role.Moderator, Role.Teacher);

            IEnumerable<Customer> customers = _customerManager.GetCustomers()
                .Where(x => x.Visits.Any(m => m.Start.Date == date.Date));

            IEnumerable<CustomerDto> dto = Mapper.Map<IEnumerable<CustomerDto>>(customers);
            return Ok(dto);
        }

        [HttpGet("addresses")]
        public IActionResult GetAddressesStat()
        {
            _authorizationService.Authorize(Role.Admin, Role.Moderator);

            IEnumerable<Customer> customers = _customerManager.GetCustomers();

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(@"Id,Status,""First name"",""Last name"",Address");

            foreach (Customer customer in customers)
            {
                string address = customer.Contacts.FirstOrDefault(x => x.Type == ContactType.Address)?.Value ?? string.Empty;
                builder.AppendLine($@"{customer.Id},{customer.Status},""{customer.FirstName}"",""{customer.LastName}"",""{address}""");
            }

            return Ok(builder.ToString());
        }

        [HttpGet("visits")]
        public IActionResult GetVisitsStat()
        {
            _authorizationService.Authorize(Role.Admin, Role.Moderator);

            IEnumerable<Customer> customers = _customerManager.GetCustomers();

            StringBuilder builder = new StringBuilder();
            builder.AppendLine(@"Id,Status,""First name"",""Last name"",Visit");

            foreach (Customer customer in customers)
            {
                foreach (Visit visit in customer.Visits)
                {
                    string date = visit.Start.ToString("dd.MM.yyy");
                    builder.AppendLine($@"{customer.Id},{customer.Status},""{customer.FirstName}"",""{customer.LastName}"",{date}");
                }
            }

            return Ok(builder.ToString());
        }

        [HttpGet("allstudentsdata")]
        public IActionResult GetAllStudentsData()
        {
            _authorizationService.Authorize(Role.Admin);

            IEnumerable<Customer> customers = _customerManager.GetCustomers();

            List<Customer> result = new List<Customer>();
            foreach (Customer customer in customers)
            {
                result.Add(_customerManager.GetCustomerById(customer.Id));
            }

            IEnumerable<CustomerDto> dto = Mapper.Map<IEnumerable<CustomerDto>>(result);
            return Ok(dto);
        }

        private readonly IAuthorizationService _authorizationService;
        private readonly ICustomerManager _customerManager;
    }
}
