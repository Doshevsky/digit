﻿using Digit.WebPortal.Core.Validation;
using Digit.WebPortal.CustomerManagement.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Digit.WebPortal.CustomerManagement
{
    public class CustomerManager : ICustomerManager
    {
        public CustomerManager(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public IEnumerable<Customer> GetCustomers()
        {
            IEnumerable<Customer> customers = _customerRepository.GetCustomers();

            return customers.Select(x =>
            {
                SortFields(x);
                CalculateAge(x);
                CheckPayment(x);
                CheckVisit(x);
                CheckAction(x);
                return x;
            });
        }
        public Customer GetCustomerById(int id)
        {
            Customer customer = _customerRepository.GetCustomerById(id);
            Require.DataObject(customer);

            SortFields(customer);
            CalculateAge(customer);
            CalculatePaymenstDays(customer);
            CalculateVisitsDuration(customer);
            CheckPayment(customer);
            CheckVisit(customer);
            CheckAction(customer);
            return customer;
        }
        public void AddCustomer(Customer customer)
        {
            customer.Status = Status.Newbie;
            customer.Created = DateTime.UtcNow;
            _customerRepository.AddCustomer(customer);
        }
        public void UpdateCustomer(Customer customer)
        {
            _customerRepository.UpdateCustomer(customer);
        }
        public void DeleteCustomer(int id)
        {
            _customerRepository.DeleteCustomer(id);
        }

        public void SetAction(int customerId, Model.Action action)
        {
            _customerRepository.SetAction(customerId, action);
        }
        public void DeleteAction(int customerId)
        {
            _customerRepository.DeleteAction(customerId);
        }

        public Representative GetRepresentativeById(int id)
        {
            return _customerRepository.GetRepresentativeById(id);
        }
        public void AddRepresentative(int customerId, Representative representative)
        {
            _customerRepository.AddRepresentative(customerId, representative);
        }
        public void UpdateRepresentative(Representative representative)
        {
            _customerRepository.UpdateRepresentative(representative);
        }
        public void DeleteRepresentative(int id)
        {
            Representative representative = _customerRepository.GetRepresentativeById(id);
            Require.DataObject(representative);

            _customerRepository.DeleteRepresentative(id);
        }

        public Comment GetCommentById(int id)
        {
            return _customerRepository.GetCommentById(id);
        }
        public void AddComment(int customerId, int authorId, Comment comment)
        {
            comment.Created = DateTime.UtcNow;
            _customerRepository.AddComment(customerId, authorId, comment);
        }
        public void UpdateComment(Comment comment)
        {
            _customerRepository.UpdateComment(comment);
        }
        public void DeleteComment(int id)
        {
            Comment comment = _customerRepository.GetCommentById(id);
            Require.DataObject(comment);

            _customerRepository.DeleteComment(id);
        }

        public void AddVisit(int customerId, Visit visit)
        {
            _customerRepository.AddVisit(customerId, visit);
        }
        public void DeleteVisit(int id)
        {
            Visit visit = _customerRepository.GetVisitById(id);
            Require.DataObject(visit);

            _customerRepository.DeleteVisit(id);
        }

        public void AddPayment(int customerId, Payment payment)
        {
            _customerRepository.AddPayment(customerId, payment);
        }
        public void DeletePayment(int id)
        {
            Payment payment = _customerRepository.GetPaymentById(id);
            Require.DataObject(payment);

            _customerRepository.DeletePayment(id);
        }

        public IEnumerable<Customer> Search(string searchQuery)
        {
            return _customerRepository.Search(searchQuery);
        }

        private void SortFields(Customer customer)
        {
            customer.Comments = customer.Comments.OrderByDescending(x => x.Created).ToList();
            customer.Visits = customer.Visits.OrderByDescending(x => x.Start).ToList();
            customer.Payments = customer.Payments.OrderByDescending(x => x.Created).ToList();
        }
        private void CalculateAge(Customer customer)
        {
            if (customer.DateOfBirth.HasValue)
            {
                DateTime today = DateTime.UtcNow;
                DateTime birthday = customer.DateOfBirth.Value;

                int age = today.Year - birthday.Year;
                customer.Age = today < birthday.AddYears(age) ? --age : age;
            }
        }
        private void CalculatePaymenstDays(Customer customer)
        {
            foreach (Payment payment in customer.Payments)
            {
                payment.Days = (payment.PeriodEnd - payment.PeriodStart).Days;
            }
        }
        private void CalculateVisitsDuration(Customer customer)
        {
            foreach (Visit visit in customer.Visits)
            {
                if (visit.End.HasValue)
                {
                    visit.Duration = visit.End.Value - visit.Start;
                }
            }
        }
        private void CheckPayment(Customer customer)
        {
            if (customer.Status != Status.Active)
            {
                return;
            }

            Payment lastPayment = customer.Payments.FirstOrDefault();
            if (lastPayment == null || lastPayment.PeriodEnd < DateTime.UtcNow)
            {
                customer.PaymentExpired = true;
            }
            else if (lastPayment.PeriodEnd <= DateTime.UtcNow + TimeSpan.FromDays(5))
            {
                customer.PaymentSoonExpire = true;
            }
        }
        private void CheckVisit(Customer customer)
        {
            if (customer.Status != Status.Active)
            {
                return;
            }

            Visit lastVisit = customer.Visits.FirstOrDefault();
            if (lastVisit == null || lastVisit.Start < DateTime.UtcNow - TimeSpan.FromDays(7))
            {
                customer.NotAttend = true;
            }
        }
        private void CheckAction(Customer customer)
        {
            if (customer.Action == null)
            {
                return;
            }

            if (customer.Action.DueDate.Date < DateTime.UtcNow.Date)
            {
                customer.Action.Overdue = true;
            }
        }

        private readonly ICustomerRepository _customerRepository;
    }
}
