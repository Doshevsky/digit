﻿using Digit.WebPortal.Core;
using Digit.WebPortal.CustomerManagement.Data;
using Microsoft.EntityFrameworkCore;

namespace Digit.WebPortal.CustomerManagement
{
    public interface ICustomerManagementDbContext : IDbContext
    {
        DbSet<CustomerData> Customers { get; }
        DbSet<ActionData> Actions { get; }
        DbSet<RepresentativeData> Representatives { get; }
        DbSet<ContactData> Contacts { get; }
        DbSet<CommentData> Comments { get; }
        DbSet<VisitData> Visits { get; }
        DbSet<PaymentData> Payments { get; }
    }
}
