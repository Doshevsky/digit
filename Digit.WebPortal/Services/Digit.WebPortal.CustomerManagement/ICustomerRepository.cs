﻿using Digit.WebPortal.CustomerManagement.Model;
using System.Collections.Generic;

namespace Digit.WebPortal.CustomerManagement
{
    public interface ICustomerRepository
    {
        IEnumerable<Customer> GetCustomers();
        Customer GetCustomerById(int id);
        void AddCustomer(Customer customer);
        void UpdateCustomer(Customer customer);
        void DeleteCustomer(int id);

        void SetAction(int customerId, Action action);
        void DeleteAction(int customerId);

        Representative GetRepresentativeById(int id);
        void AddRepresentative(int customerId, Representative representative);
        void UpdateRepresentative(Representative representative);
        void DeleteRepresentative(int id);

        Visit GetVisitById(int id);
        void AddVisit(int customerId, Visit visit);
        void DeleteVisit(int id);

        Comment GetCommentById(int id);
        void AddComment(int customerId, int authorId, Comment comment);
        void UpdateComment(Comment comment);
        void DeleteComment(int id);

        Payment GetPaymentById(int id);
        void AddPayment(int customerId, Payment payment);
        void DeletePayment(int id);

        IEnumerable<Customer> Search(string searchQuery);
    }
}
