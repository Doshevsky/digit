﻿using System.Collections.Generic;

namespace Digit.ContentManagement.Models
{
    public class Topic
    {
        public Topic()
        {
            Subtopics = new List<Topic>();
        }

        public string Id { get; set; }
        public string[] Path { get; set; }
        public int Number { get; set; }
        public string Title { get; set; }
        public List<Topic> Subtopics { get; set; }
        public bool IsArticle { get; set; }
    }
}
