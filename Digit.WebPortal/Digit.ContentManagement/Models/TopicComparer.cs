﻿using System.Collections.Generic;

namespace Digit.ContentManagement.Models
{
    public class TopicComparer : IComparer<Topic>
    {
        static TopicComparer()
        {
            Instance = new TopicComparer();
        }

        public static TopicComparer Instance { get; }

        public int Compare(Topic x, Topic y)
        {
            if (x.Number == y.Number)
            {
                return x.Title.CompareTo(y.Title);
            }

            if (x.Number > y.Number)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }
    }
}
