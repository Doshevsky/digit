﻿using Digit.ContentManagement.FileStorage;
using Digit.Modularity;
using Microsoft.Extensions.DependencyInjection;

namespace Digit.ContentManagement
{
    public sealed class ContentManagementModule : IModule
    {
        public void Configure(IServiceCollection services)
        {
            services.AddScoped<IFileStorage, AzureFileStorage>();
            services.AddScoped<IContentManager, ContentManager>();
        }
        public void Start()
        {
        }
        public void Stop()
        {
        }
    }
}
