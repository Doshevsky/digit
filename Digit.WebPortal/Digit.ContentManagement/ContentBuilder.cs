﻿using CommonMark;
using Digit.ContentManagement.FileStorage;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Digit.ContentManagement
{
    internal class ContentBuilder
    {
        public ContentBuilder(IFileStorage fileStorage)
        {
            _fileStorage = fileStorage;
        }

        public async Task<string> Build(string data, string basePath)
        {
            try
            {
                string html = CommonMarkConverter.Convert(data);
                HtmlDocument document = new HtmlDocument();
                document.LoadHtml(html);

                ReplaceCodeBlocks(document.DocumentNode);
                await EmbedImages(document.DocumentNode, basePath);
                AddBages(document.DocumentNode);

                return document.DocumentNode.OuterHtml;
            }
            catch (Exception ex)
            {
                //TODO: throw correct exception
                throw new Exception("Failed to build article", ex);
            }
        }

        private void ReplaceCodeBlocks(HtmlNode rootNode)
        {
            IEnumerable<HtmlNode> nodes = SelectNodes(rootNode, "//code");
            foreach (HtmlNode node in nodes)
            {
                HtmlAttribute srcAttribute = node.Attributes["class"];
                string srcValue = srcAttribute?.Value;
                if (string.Equals(srcValue, "language-csharp", StringComparison.OrdinalIgnoreCase))
                {
                    srcAttribute.Value = "csharp highlight";
                }
            }
        }
        private async Task EmbedImages(HtmlNode rootNode, string basePath)
        {
            IEnumerable<HtmlNode> nodes = SelectNodes(rootNode, "//img");
            foreach (HtmlNode node in nodes)
            {
                HtmlAttribute srcAttribute = node.Attributes["src"];
                string srcValue = srcAttribute?.Value;
                if (!string.IsNullOrEmpty(srcValue))
                {
                    string imagePath = Path.Combine(basePath, srcValue);
                    string base64image = await GetBase64Image(imagePath);
                    srcAttribute.Value = $"data:image/png;base64,{base64image}";
                }
            }
        }
        private void AddBages(HtmlNode rootNode)
        {
            string html = rootNode.InnerHtml;
            html = html
                .Replace("[problem 1]", @"<span class=""badge badge-pill badge-primary"">1</span>")
                .Replace("[problem 2]", @"<span class=""badge badge-pill badge-warning"">2</span>")
                .Replace("[problem 3]", @"<span class=""badge badge-pill badge-danger"">3</span>")
                .Replace("[project 1]", @"<span class=""badge badge-pill badge-primary"">1</span>")
                .Replace("[project 2]", @"<span class=""badge badge-pill badge-warning"">2</span>")
                .Replace("[project 3]", @"<span class=""badge badge-pill badge-danger"">3</span>");

            rootNode.InnerHtml = html;
        }

        private IEnumerable<HtmlNode> SelectNodes(HtmlNode parent, string xpath)
        {
            HtmlNodeCollection nodes = parent.SelectNodes(xpath);
            return nodes ?? Enumerable.Empty<HtmlNode>();
        }

        private async Task<string> GetBase64Image(string path)
        {
            byte[] bytes = await _fileStorage.GetBinaryFileContentAsync(path);
            return Convert.ToBase64String(bytes);
        }

        private readonly IFileStorage _fileStorage;
    }
}
