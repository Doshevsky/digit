﻿using Digit.ContentManagement.FileStorage;
using Digit.ContentManagement.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Digit.ContentManagement
{
    internal class ContentManager : IContentManager
    {
        public ContentManager(IFileStorage fileStorage)
        {
            _contentBuilder = new ContentBuilder(fileStorage);
            _storageProvider = fileStorage;
        }

        public async Task<IEnumerable<Topic>> GetTopicsAsync()
        {
            List<Topic> topics = new List<Topic>();
            IEnumerable<FileStorageItem> storageItems = await _storageProvider.GetItemsRecursivelyAsync();
            FillTopics(storageItems, topics);

            return topics;
        }
        public async Task<Article> GetArticleAsync(string id)
        {
            string path = Decode(id);
            string basePath = GetBasePath(path);

            string fileContent = await _storageProvider.GetFileContentAsync(path);
            return await BuildArticle(fileContent, basePath);
        }
        public async Task<Article> CreateArticleAsync(string data)
        {
            return await BuildArticle(data, null);
        }

        private void FillTopics(IEnumerable<FileStorageItem> source, List<Topic> target)
        {
            foreach (FileStorageItem item in source)
            {
                if (item.Name.StartsWith('_'))
                {
                    continue;
                }

                Topic topic = new Topic
                {
                    Id = Encode(item.FullPath),
                    Path = item.PathSegments,
                    Number = GetTopicNumber(item.Name)
                };

                FillTopic(topic, item);
                target.Add(topic);
            }

            target.Sort(TopicComparer.Instance);
        }
        private void FillTopic(Topic topic, FileStorageItem item)
        {
            switch (item.ItemType)
            {
                case FileStorageItemType.File:
                    topic.Title = Path.GetFileNameWithoutExtension(item.Name);
                    topic.IsArticle = true;
                    break;

                case FileStorageItemType.Direcory:
                    topic.Title = item.Name;
                    FillTopics(item.Subitems, topic.Subtopics);
                    break;
            }
        }

        private string Encode(string value)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(value);
            string base64value = Convert.ToBase64String(bytes);
            return WebUtility.UrlEncode(base64value);
        }
        private string Decode(string encodedValue)
        {
            byte[] bytes = Convert.FromBase64String(encodedValue);
            string value = Encoding.Unicode.GetString(bytes);
            return WebUtility.UrlDecode(value);
        }

        private string GetBasePath(string path)
        {
            int index = path.LastIndexOf('/');
            return path.Substring(0, index + 1);
        }
        private int GetTopicNumber(string fileName)
        {
            string[] parts = fileName.Split('.');
            if (parts.Length >= 2 && int.TryParse(parts[0], out int number))
            {
                return number;
            }

            return 0;
        }

        private async Task<Article> BuildArticle(string data, string basePath)
        {
            string content = await _contentBuilder.Build(data, basePath);
            return new Article
            {
                Content = content
            };
        }

        private readonly ContentBuilder _contentBuilder;
        private readonly IFileStorage _storageProvider;
    }
}
