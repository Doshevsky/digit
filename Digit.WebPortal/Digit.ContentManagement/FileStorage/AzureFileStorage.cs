﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.File;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Digit.ContentManagement.FileStorage
{
    internal class AzureFileStorage : IFileStorage
    {
        public AzureFileStorage()
        {
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConnectionString);
            CloudFileClient fileClient = storageAccount.CreateCloudFileClient();
            _share = fileClient.GetShareReference(FileShareName);
            _credentials = fileClient.Credentials;
        }

        public async Task<IEnumerable<FileStorageItem>> GetItemsRecursivelyAsync()
        {
            bool shareExists = await _share.ExistsAsync();
            if (!shareExists)
            {
                throw new Exception($"Share '{FileShareName}' not exist.");
            }

            CloudFileDirectory rootDirectory = _share.GetRootDirectoryReference();
            List<FileStorageItem> items = new List<FileStorageItem>();
            await FillStorageItems(rootDirectory, items);

            return items;
        }
        public async Task<string> GetFileContentAsync(string path)
        {
            CloudFile file = new CloudFile(new Uri(path), _credentials);
            bool fileExist = await file.ExistsAsync();
            if (!fileExist)
            {
                throw new Exception($"File '{path}' not exist");
            }

            return await file.DownloadTextAsync();
        }
        public async Task<byte[]> GetBinaryFileContentAsync(string path)
        {
            CloudFile file = await GetFileOrThrow(path);
            using (MemoryStream stream = new MemoryStream())
            {
                await file.DownloadToStreamAsync(stream);
                return stream.ToArray();
            }
        }

        private async Task FillStorageItems(CloudFileDirectory rootDirectory, List<FileStorageItem> result)
        {
            List<IListFileItem> listFileItems = await ListFilesAndDirectories(rootDirectory);
            foreach (IListFileItem listItem in listFileItems)
            {
                FileStorageItem storageItem = new FileStorageItem
                {
                    FullPath = listItem.Uri.ToString(),
                    PathSegments = GetPathSegments(listItem.Uri),
                    ItemType = FileStorageItemType.Other
                };

                if (listItem is CloudFileDirectory directory)
                {
                    storageItem.ItemType = FileStorageItemType.Direcory;
                    storageItem.Name = directory.Name;
                    await FillStorageItems(directory, storageItem.Subitems);
                }
                else if (listItem is CloudFile file)
                {
                    storageItem.ItemType = FileStorageItemType.File;
                    storageItem.Name = file.Name;
                }

                result.Add(storageItem);
            }
        }
        private async Task<List<IListFileItem>> ListFilesAndDirectories(CloudFileDirectory directory)
        {
            List<IListFileItem> listFileItems = new List<IListFileItem>();
            FileContinuationToken token = null;
            do
            {
                FileResultSegment resultSegment = await directory.ListFilesAndDirectoriesSegmentedAsync(token);
                listFileItems.AddRange(resultSegment.Results);
                token = resultSegment.ContinuationToken;
            }
            while (token != null);

            return listFileItems;
        }

        private string[] GetPathSegments(Uri uri)
        {
            return uri.LocalPath.Split('/')
                .Skip(2)
                .SkipLast(1)
                .ToArray();
        }

        private async Task<CloudFile> GetFileOrThrow(string path)
        {
            CloudFile file = new CloudFile(new Uri(path), _credentials);
            bool fileExist = await file.ExistsAsync();
            if (!fileExist)
            {
                throw new Exception($"File '{path}' not exist");
            }

            return file;
        }

        private readonly CloudFileShare _share;
        private readonly StorageCredentials _credentials;

        private const string FileShareName = "digit-learning-content";

        // TODO: Move to configuration/appSettings
        private const string ConnectionString = "DefaultEndpointsProtocol=https;AccountName=digit;AccountKey=tjlhjtCHslmZD/3a9P38xQBOmde1p507TJJv/gLXiwlgFAs9/HWIamvC4ZhEmu3lre3qUsH4+BJk1CB9hTb9dA==;EndpointSuffix=core.windows.net";
    }
}
