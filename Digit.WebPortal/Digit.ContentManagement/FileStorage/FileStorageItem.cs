﻿using System.Collections.Generic;

namespace Digit.ContentManagement.FileStorage
{
    public class FileStorageItem
    {
        public FileStorageItem()
        {
            Subitems = new List<FileStorageItem>();
        }

        public string FullPath { get; set; }
        public string[] PathSegments { get; set; }
        public string Name { get; set; }
        public FileStorageItemType ItemType { get; set; }
        public List<FileStorageItem> Subitems { get; set; }
    }
}
