﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Digit.ContentManagement.FileStorage
{
    public interface IFileStorage
    {
        Task<IEnumerable<FileStorageItem>> GetItemsRecursivelyAsync();
        Task<string> GetFileContentAsync(string path);
        Task<byte[]> GetBinaryFileContentAsync(string path);
    }
}
