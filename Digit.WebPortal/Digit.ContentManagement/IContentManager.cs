﻿using Digit.ContentManagement.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Digit.ContentManagement
{
    public interface IContentManager
    {
        Task<IEnumerable<Topic>> GetTopicsAsync();
        Task<Article> GetArticleAsync(string id);
        Task<Article> CreateArticleAsync(string data);
    }
}
