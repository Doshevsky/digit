import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Topic } from '../model/topic';
import { Article } from '../model/article';

@Injectable()
export class LearningContentApiService {
  public constructor(
    private http: HttpClient) {
  }

  public getTopics(): Observable<Topic[]> {
    return this.http.get<Topic[]>(this.contentUrl);
  }

  public getArticle(id: string): Observable<Article> {
    return this.http.get<Article>(`${this.contentUrl}/article?id=${id}`);
  }

  public testArticle(data: string): Observable<Article> {
    const body = { data: data };
    return this.http.post<Article>(`${this.contentUrl}/article/test`, body);
  }

  private readonly contentUrl: string = 'api/content';
}
