import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { HighlightJsModule, HighlightJsService } from 'angular2-highlight-js';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { LearningContentApiService } from './api/learning-content-api.service';

import { LearningContentService } from './model/learning-content.service';

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    HighlightJsModule,
    AppRoutingModule
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    HighlightJsService,
    LearningContentApiService,
    LearningContentService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
