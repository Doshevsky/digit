import { Component, OnInit } from '@angular/core';

import { Topic } from '../../model/topic';
import { LearningContentService } from '../../model/learning-content.service';
import { Article } from '../../model/article';

@Component({
  selector: 'app-learning-content',
  templateUrl: './learning-content.component.html',
  styleUrls: ['./learning-content.component.scss']
})
export class LearningContentComponent implements OnInit {
  constructor(
    private learningContentService: LearningContentService) {
  }

  public topic: Topic;
  public article: Article;

  ngOnInit(): void {
    this.learningContentService.activeTopic.subscribe((topic: Topic) => {
      this.topic = topic;
    });

    this.learningContentService.activeArticle.subscribe((article: Article) => {
      this.article = article;
    });
  }
}
