import { Component, OnInit } from '@angular/core';

import { Article } from '../../../model/article';
import { LearningContentApiService } from '../../../api/learning-content-api.service';

@Component({
  selector: 'app-article-test',
  templateUrl: './article-test.component.html',
  styleUrls: ['../article/article.component.scss']
})
export class ArticleTestComponent implements OnInit {
  constructor(
    private learningContentApiService: LearningContentApiService) {
    this.reader = new FileReader();
    this.reader.onloadend = event => this.readerOnLoadend(event);
  }

  article: Article;

  ngOnInit() {
  }

  fileChange(event: any) {
    this.article = null;

    let fileList: FileList = event.target.files;
    if (fileList.length == 0) {
      return;
    }

    let file: File = fileList[0];
    this.reader.readAsText(file);
  }

  private readerOnLoadend(event: any) {
    this.learningContentApiService.testArticle(this.reader.result)
      .subscribe(article => this.article = article);
  }

  private reader: FileReader;
}
