import { Component, Input, ElementRef, OnInit, AfterViewInit } from '@angular/core';
import { HighlightJsService } from 'angular2-highlight-js';

import { Article } from '../../../model/article';
import { LearningContentService } from '../../../model/learning-content.service';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit, AfterViewInit {
  constructor(
    private highlightJsService: HighlightJsService,
    private learningContentService: LearningContentService,
    private el: ElementRef) {
  }

  @Input()
  public article: Article;

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    let codeBlocks: any[] = this.el.nativeElement.querySelectorAll('.highlight');
    for (let codeBlock of codeBlocks) {
      this.highlightJsService.highlight(codeBlock);
    }
  }
}
