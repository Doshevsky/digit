import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LearningContentRoutingModule } from './learning-content-routing.module';
import { LearningContentComponent } from './learning-content.component';
import { ArticleComponent } from './article/article.component';
import { ArticleTestComponent } from './article-test/article-test.component';

@NgModule({
  imports: [
    CommonModule,
    LearningContentRoutingModule
  ],
  declarations: [LearningContentComponent, ArticleComponent, ArticleTestComponent]
})
export class LearningContentModule {
}
