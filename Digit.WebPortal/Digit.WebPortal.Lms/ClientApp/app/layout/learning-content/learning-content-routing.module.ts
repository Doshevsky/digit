import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LearningContentComponent } from './learning-content.component';
import { ArticleComponent } from './article/article.component';
import { ArticleTestComponent } from './article-test/article-test.component';

const routes: Routes = [
  { path: '', component: LearningContentComponent },
  { path: 'test', component: ArticleTestComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LearningContentRoutingModule {
}
