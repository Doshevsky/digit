import { Component, Input, OnInit } from '@angular/core';

import { Topic } from '../../../../model/topic';
import { LearningContentService } from '../../../../model/learning-content.service';
import { MenuItem } from '../menu-item';

@Component({
  selector: 'app-learning-content-menu',
  templateUrl: './learning-content-menu.component.html',
  styleUrls: ['./learning-content-menu.component.scss']
})
export class LearningContentMenuComponent implements OnInit {
  public constructor(
    private learningContentService: LearningContentService) {
  }

  public menuItems: MenuItem[];

  public ngOnInit(): void {
    this.learningContentService.topics.subscribe((topics: Topic[]) => {
      this.menuItems = new Array<MenuItem>();
      this.fillMenu(this.menuItems, topics);
    });
  }

  public onMenuItemSelected(item: MenuItem): void {
    let selectedTopic: Topic = item.element;
    item.toggle();
    this.learningContentService.selectTopic(item.element);
  }

  private fillMenu(menuItems: MenuItem[], topics: Topic[]): void {
    for (let topic of topics) {
      let menuItem: MenuItem = new MenuItem(topic);
      this.fillMenu(menuItem.subitems, topic.subtopics);
      menuItems.push(menuItem);
    }
  }
}
