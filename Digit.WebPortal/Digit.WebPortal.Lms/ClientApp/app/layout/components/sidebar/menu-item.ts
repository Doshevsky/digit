export class MenuItem {
    public constructor(element: any) {
        this.element = element;
        this.expanded = false;
        this.subitems = new Array<MenuItem>();
    }

    public readonly element: any;
    public expanded: boolean;
    public subitems: MenuItem[];

    public toggle() {
        if (this.subitems.length > 0) {
            this.expanded = !this.expanded;
        }
        else {
            this.expanded = false;
        }
    }
}
