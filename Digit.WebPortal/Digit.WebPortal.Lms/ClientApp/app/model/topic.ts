export class Topic {
    id: string;
    path: string[]
    title: string;
    subtopics: Topic[];
    isArticle: boolean;
}