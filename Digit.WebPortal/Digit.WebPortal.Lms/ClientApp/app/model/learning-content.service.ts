import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

import { Topic } from './topic';
import { Article } from './article';
import { LearningContentApiService } from '../api/learning-content-api.service'

@Injectable()
export class LearningContentService {
  public constructor(
    private api: LearningContentApiService) {

    this.articleSource = new BehaviorSubject<Article>(null);
    this.topicSource = new BehaviorSubject<Topic>(null);

    this.topics = this.api.getTopics();
    this.activeArticle = this.articleSource.asObservable();
    this.activeTopic = this.topicSource.asObservable();
  }

  public topics: Observable<Topic[]>;
  public activeTopic: Observable<Topic>;
  public activeArticle: Observable<Article>;

  public selectTopic(topic: Topic): void {
    this.topicSource.next(topic);
    if (topic.isArticle) {
      this.loadArticle(topic.id);
    }
  }

  private loadArticle(id: string): void {
    this.articleSource.next(null);
    this.api.getArticle(id).subscribe((article: Article) => {
      this.articleSource.next(article);
    });
  }

  private articleSource: BehaviorSubject<Article>;
  private topicSource: BehaviorSubject<Topic>;
}
