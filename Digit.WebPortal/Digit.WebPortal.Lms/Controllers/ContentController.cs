﻿using AutoMapper;
using Digit.ContentManagement;
using Digit.ContentManagement.Models;
using Digit.WebPortal.Lms.Dto.Content;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Digit.WebPortal.Lms.Controllers
{
    [Route("api/content")]
    public class ContentController : Controller
    {
        public ContentController(IContentManager contentManager)
        {
            _contentManager = contentManager;
        }

        // api/content
        [HttpGet]
        public async Task<IActionResult> GetTopics()
        {
            IEnumerable<Topic> topics = await _contentManager.GetTopicsAsync();

            IEnumerable<TopicDto> dto = Mapper.Map<IEnumerable<TopicDto>>(topics);
            return Ok(dto);
        }

        // api/content/article?id=<id>
        [HttpGet("article")]
        public async Task<IActionResult> GetArticle(string id)
        {
            Article article = await _contentManager.GetArticleAsync(id);

            ArticleDto dto = Mapper.Map<ArticleDto>(article);
            return Ok(dto);
        }

        // api/content/article/test
        [HttpPost("article/test")]
        public async Task<IActionResult> TestArticle([FromBody] ArticleTestDto dto)
        {
            if (dto == null || string.IsNullOrWhiteSpace(dto.Data))
            {
                return BadRequest();
            }

            Article article = await _contentManager.CreateArticleAsync(dto.Data);

            ArticleDto articleDto = Mapper.Map<ArticleDto>(article);
            return Ok(articleDto);
        }

        private readonly IContentManager _contentManager;
    }
}
