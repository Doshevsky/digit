﻿namespace Digit.WebPortal.Lms.Dto.Content
{
    public class ArticleDto
    {
        public string Content { get; set; }
    }
}
