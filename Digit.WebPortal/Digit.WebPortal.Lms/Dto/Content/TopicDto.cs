﻿using System.Collections.Generic;

namespace Digit.WebPortal.Lms.Dto.Content
{
    public class TopicDto
    {
        public TopicDto()
        {
            Subtopics = new List<TopicDto>();
        }

        public string Id { get; set; }
        public string[] Path { get; set; }
        public string Title { get; set; }
        public List<TopicDto> Subtopics { get; set; }
        public bool IsArticle { get; set; }
    }
}
