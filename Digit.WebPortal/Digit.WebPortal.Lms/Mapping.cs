﻿using Digit.ContentManagement.Models;
using Digit.WebPortal.Lms.Dto.Content;

namespace Digit.WebPortal.Lms
{
    public static class Mapping
    {
        public static void CreateMapping()
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Topic, TopicDto>();
                cfg.CreateMap<Article, ArticleDto>();
            });

            AutoMapper.Mapper.AssertConfigurationIsValid();
        }
    }
}
